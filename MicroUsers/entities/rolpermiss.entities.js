const Sequelize = require('sequelize')
const sequelize = require('../config/settings.config')

const User = sequelize.define('Users', {
    Roles_idRoles: Sequelize.INTEGER,
    Permissions_idPermissions: Sequelize.INTEGER
},
{
    timestamps: false
})

module.exports = User