const Sequelize = require('sequelize')
const sequelize = require('../config/settings.config')

const User = sequelize.define('Users', {
    idUsers: {type: Sequelize.SMALLINT, primaryKey: true},
    Email: Sequelize.STRING,
    Name: Sequelize.STRING,
    Password: Sequelize.STRING,
    Tipo_Documento_idTipo_Documento: Sequelize.INTEGER,
    Roles_idRoles: Sequelize.INTEGER,
    TipoPersona_idTipoPersona: Sequelize.INTEGER
},
{
    timestamps: false
})

module.exports = User