const User = require('../entities/auth.entities')
const crypto = require('crypto');
const env = require('dotenv')
env.config()
const secret = process.env.PASS_DANNY
/*Logger
    'fatal', 'error', 'warn', 'info', 'debug'
*/
var logger = require('logger').createLogger('development.log'); // logs to a file
const mail = require('../function/mail.funtions')

const authModel = {}

authModel.auth = async (userData) => {
  let pass = Buffer.from(userData.Password).toLocaleString('base64')
  let hash = crypto.createHmac('sha256', secret)
    .update(pass)
    .digest('hex');
  return await User.findAll({
    where: {
      idUsers: userData.idUsers,
      Password: hash
    }
  }).then(data => {
    logger.info('Se realizo la consulta de forma exitosa')
    if (data.length === 0) {
      return {
        data,
        mensaje: 'Por favor rectifiqué los datos ingresados.'
      }
    } else {
      console.log('Danniel ', data)
      return {
        data,
        mensaje: 'Consulta realizada con exito'
      }
    }
  }).catch(err => {
    logger.error(err)
    return {
      error: err,
      mensaje: 'No se puede procesar la solicitud'
    }
  })
}


authModel.createUser = async (userData) => {
  return await User.findAll({
    where: {
      idUsers: userData.idUsers
    }
  }).then(data => {
    console.log('Resultados Danny ', data)
    if (data.length === 0) {
      let pass = Buffer.from(userData.Password).toLocaleString('base64');
      let hash = crypto.createHmac('sha256', secret)
        .update(pass)
        .digest('hex');
      let jsonUser = {
        'idUsers': userData.idUsers,
        'Email': userData.Email,
        'Name': userData.Name,
        'Password': hash,
        'Tipo_Documento_idTipo_Documento': userData.Tipo_Documento_idTipo_Documento,
        'Roles_idRoles': userData.Roles_idRoles,
        'TipoPersona_idTipoPersona': userData.TipoPersona_idTipoPersona
      };
      return User.create(jsonUser).then(data => {
        return {
          data,
          mensaje: 'Creo correctamente el usuario'
        }
      }).catch(err => {
        logger.error(err)
        return {
          error: err,
          mensaje: 'No se puede procesar la solicitud'
        }
      })
    } else {
      return {
        data,
        mensaje: 'El usuario ya se encuentra registrado'
      }
    }
  })

  
}

authModel.recoveryPassword = async userData => {
  return await User.findAll({
    where: {
      'idUsers': userData.idUsers,
      'Tipo_Documento_idTipo_Documento': userData.Tipo_Documento_idTipo_Documento
    }
  }).then(data => {
    logger.info('Se encontro el usuario!')
    if (data.length === 0) {
      return {
        mensaje: 'No se encuentra un usuario asociado al documento enviado.'
      }
    } else {
      let returnMail = mail.sendMail(data);
      return {
        data,
        mensaje: 'Se ha envíado un correo a la cuenta asociada.'
      }
    }
  }).catch(err => {
    logger.info(err)
    return {
      err,
      mensaje: 'No se puede realizar la consulta'
    }
  });
}

module.exports = authModel