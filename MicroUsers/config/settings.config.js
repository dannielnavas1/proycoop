const env = require('dotenv')
env.config()

const Sequelize = require('sequelize')

const sequelize = new Sequelize(process.env.DB_DB, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: 'mysql'
})

sequelize.authenticate()
    .then(data=>{
        console.log('Se realiza conexión')
        // console.log(data)
    }).catch(error=>{
        console.log('Se realiza conexión')
        console.log(error)
    })

module.exports = sequelize