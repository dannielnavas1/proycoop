export const environment = {
  production: true,
  urlUser: 'https://micro.dannielnavas.com.co',
  urlAsociado: 'https://apiasociado.dannielnavas.com.co',
  urlOrganizacion: 'https://apiorganizacion.dannielnavas.com.co',
  // urlUser: 'http://127.0.0.1:3002',
  // urlAsociado: 'http://localhost:3010',
  // urlOrganizacion: 'http://localhost:3000',
  firebaseConfig: {
    apiKey: "AIzaSyCJkvWahvMJ1sZ-h7l9ybxuRcdQjPULWAk",
    authDomain: "cooperativasapp-8d16d.firebaseapp.com",
    databaseURL: "https://cooperativasapp-8d16d.firebaseio.com",
    projectId: "cooperativasapp-8d16d",
    storageBucket: "cooperativasapp-8d16d.appspot.com",
    messagingSenderId: "291596006860",
    appId: "1:291596006860:web:cf9691a72eec1398"
  }
};
