// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlUser: 'https://micro.dannielnavas.com.co',
  urlAsociado: 'https://apiasociado.dannielnavas.com.co',
  urlOrganizacion: 'https://apiorganizacion.dannielnavas.com.co',
  // urlUser: 'http://127.0.0.1:3002',
  // urlAsociado: 'http://localhost:3010',
  // urlOrganizacion: 'http://localhost:3000',
  firebaseConfig: {
    apiKey: "AIzaSyCJkvWahvMJ1sZ-h7l9ybxuRcdQjPULWAk",
    authDomain: "cooperativasapp-8d16d.firebaseapp.com",
    databaseURL: "https://cooperativasapp-8d16d.firebaseio.com",
    projectId: "cooperativasapp-8d16d",
    storageBucket: "cooperativasapp-8d16d.appspot.com",
    messagingSenderId: "291596006860",
    appId: "1:291596006860:web:cf9691a72eec1398"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
