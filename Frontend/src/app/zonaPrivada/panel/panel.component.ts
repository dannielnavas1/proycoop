import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.less']
})
export class PanelComponent implements OnInit {
  user: any;
  userInfo: any;

  constructor(private cookie: CookieService) {
    // this.cookie.deleteAll();
    
    this.user = JSON.parse(this.cookie.get('userAcount'));
    for(let info of this.user.data){
      this.userInfo = info;
    }
    console.log(this.userInfo)
  }

  ngOnInit() {

  }

}
