import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-informacionbasica',
  templateUrl: './informacionbasica.component.html',
  styleUrls: ['./informacionbasica.component.less']
})
export class InformacionbasicaComponent implements OnInit {
  informacionbasica: FormGroup;
  data: any;
  infoUser: any;
  verCampo: false;

  constructor(private fb: FormBuilder,
              private cookie: CookieService,
              private router: Router,
              private spinner: NgxSpinnerService,
              private serviceAsociadoService: ServiceAsociadoService) {
                this.spinner.show();
              }

  ngOnInit() {
    this.informacionbasica = this.fb.group({
      tipoDocumento: ['', [Validators.required]],
      documento: ['', [Validators.required]],
      nombreRazon: ['', [Validators.required]],
      tipoPersona: ['', [Validators.required]]
    });
    this.infoUser = JSON.parse(this.cookie.get('userAcount'));
    console.log(this.infoUser)
    for (let u of this.infoUser.data) {
      this.informacionbasica.controls['tipoDocumento'].setValue(u.Tipo_Documento_idTipo_Documento);
      this.informacionbasica.controls['documento'].setValue(u.idUsers);
      this.informacionbasica.controls['nombreRazon'].setValue(u.Name);
      u.Tipo_Documento_idTipo_Documento === 1 ?
      this.informacionbasica.controls['tipoPersona'].setValue('1') :
      this.informacionbasica.controls['tipoPersona'].setValue('2')
    }
    this.onSubmit()
  }

  onSubmit() {
    if (!this.informacionbasica.invalid) {
      this.cookie.set('basico', btoa(JSON.stringify(this.informacionbasica.value)));
      this.serviceAsociadoService.consultaBasico(this.informacionbasica.value.documento).subscribe(data => {
        this.cookie.set('idIdentUser', btoa(this.informacionbasica.value.documento));
        this.data = JSON.parse(this.cookie.get('userAcount'));
        if (this.data.data[0].TipoPersona_idTipoPersona === 1) {
          this.router.navigate(['/natural']);
          this.spinner.hide();
        } else {
          this.router.navigate(['/juridica']);
          this.spinner.hide();
        }
      }, err => {
        this.serviceAsociadoService.registroDatosBasicos(this.informacionbasica.value).subscribe(data => {
          this.data = JSON.parse(this.cookie.get('userAcount'));
          if (this.data.data[0].TipoPersona_idTipoPersona === 1) {
            this.router.navigate(['/natural']);
            this.spinner.hide();
          } else {
            this.router.navigate(['/juridica']);
            this.spinner.hide();
          }
        }, error => {
          alert('No es posible registrar el asociado');
        });
      });
    }
  }

}
