import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-beneficiarios',
  templateUrl: './beneficiarios.component.html',
  styleUrls: ['./beneficiarios.component.less']
})
export class BeneficiariosComponent implements OnInit {
  beneficiarios: FormGroup;
  data: any;
  almacenBeneficiarios: string[] = [];
  verResumen: boolean = false;
  viewEdit: boolean;
  informacion: any;
  searchData: boolean;
  update: boolean;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
              private serviceAsociadoService: ServiceAsociadoService,
              private cookie: CookieService,
              private router: Router) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
                console.log(this.dataInfo)
                this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
              }

  ngOnInit() {
    this.beneficiarios = this.fb.group({
      tipoDocumento: ['', [Validators.required]],
      numeroDocumento: ['', [Validators.required]],
      primerNombre: ['', [Validators.required]],
      segundoNombre: [''],
      primerApellido: ['', [Validators.required]],
      segundoApellido: [''],
      fechaNacimiento: ['', [Validators.required]],
      genero: ['', [Validators.required]],
      relNucleo: ['', [Validators.required]],
      nivelEducativo: ['', [Validators.required]],
      seguridadSocial: ['', [Validators.required]],
      fechaRetiro: [''],
      causalRetiro: [''],
      cualOtra: ['']
    });
    this.searchData = this.cookie.check('idIdentUser');
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
      }
    } else {
      this.serviceAsociadoService.consultarDatosBeneficiarios(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          // this.natural.controls['primerNombre'].setValue(info.PrimerNombre);
          this.beneficiarios.controls['numeroDocumento'].setValue(info.idBeneficiario);
          this.beneficiarios.controls['primerNombre'].setValue(info.PrimerNombre);
          this.beneficiarios.controls['segundoNombre'].setValue(info.SegundoNombre);
          this.beneficiarios.controls['primerApellido'].setValue(info.PrimerApellido);
          this.beneficiarios.controls['segundoApellido'].setValue(info.SegundoApellido);
          this.beneficiarios.controls['fechaNacimiento'].setValue(info.FechaNacimiento);
          this.beneficiarios.controls['fechaRetiro'].setValue(info.FechaRetiro);
          this.beneficiarios.controls['cualOtra'].setValue(info.Cual);
        }
      }, err => {
        console.log(err);
      });
    }
  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (this.almacenBeneficiarios.length !== 0) {
      for (let beneficiario of this.almacenBeneficiarios) {
        this.serviceAsociadoService.registroBeneficiarios(beneficiario, this.data.documento).subscribe(data => {
          this.router.navigate(['/contacto']);
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      }
      }
    }

  addBeneficiario() {
    this.almacenBeneficiarios.push(this.beneficiarios.value);
    this.verResumen =  true;
    console.log(this.almacenBeneficiarios);
    this.beneficiarios.reset();
  }

  deleteBeneficiario(i) {
    this.almacenBeneficiarios.splice(i, 1);
    console.log(this.almacenBeneficiarios);
    if (this.almacenBeneficiarios.length === 0) {
      this.verResumen =  false;
    }
  }

  mostrarFormulario() {
    this.verResumen =  false;
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
