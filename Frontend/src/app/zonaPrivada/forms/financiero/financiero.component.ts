import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-financiero',
  templateUrl: './financiero.component.html',
  styleUrls: ['./financiero.component.less']
})
export class FinancieroComponent implements OnInit {
  financiera: FormGroup;
  params: any;
  data: any;
  viewEdit: boolean;
  searchData: boolean;
  informacion: any;
  update: boolean;
  dataPersona: any;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
    private serviceAsociadoService: ServiceAsociadoService,
    private cookie: CookieService,
    private router: Router) {
      this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
      console.log(this.dataInfo)
      this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
    }

  ngOnInit() {
    this.financiera = this.fb.group({
      utilizacionCredito: [''],
      cualCredito: [''],
      valorDesembolsoCredito: [''],
      tasaCredito: [''],
      fechaDesembolsoCredito: [''],
      utilizacionahorro: [''],
      cualahorro: [''],
      valorDesembolsoahorro: [''],
      tasaahorro: [''],
      fechaDesembolsoahorro: [''],
      utilizacionApoyos: [''],
      cualApoyos: [''],
      valorDesembolsoApoyos: [''],
      fechaDesembolsoApoyos: [''],
      utilizacionConvenios: [''],
      cualConvenios: [''],
      valorDesembolsoConvenios: [''],
      fechaDesembolsoConvenios: [''],
    });
    this.searchData = this.cookie.check('idIdentUser');
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
        this.dataPersona = JSON.parse(atob(this.cookie.get('basico')));
        // this.dataNombre = this.dataPersona.nombreRazon.split(' ');
        // this.natural.controls['primerNombre'].setValue(this.dataNombre[0]);
        // this.data = JSON.parse(atob(this.cookie.get('basico')));
      }
    } else {
      this.serviceAsociadoService.consultarDatoscursosFinancieros(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          if (info.TipoFinanciero_idTipoFinanciero === 1) {
            this.financiera.controls['cualCredito'].setValue(info.Cual);
            this.financiera.controls['valorDesembolsoCredito'].setValue(info.ValorDesembolso);
            this.financiera.controls['tasaCredito'].setValue(info.Tasa);
            this.financiera.controls['fechaDesembolsoCredito'].setValue(info.FechaDesembolso);
          } else if (info.TipoFinanciero_idTipoFinanciero === 2) {
            this.financiera.controls['cualahorro'].setValue(info.Cual);
            this.financiera.controls['valorDesembolsoahorro'].setValue(info.ValorDesembolso);
            this.financiera.controls['tasaahorro'].setValue(info.Tasa);
            this.financiera.controls['fechaDesembolsoahorro'].setValue(info.FechaDesembolso);
          } else if (info.TipoFinanciero_idTipoFinanciero === 3) {
            this.financiera.controls['cualApoyos'].setValue(info.Cual);
            this.financiera.controls['valorDesembolsoApoyos'].setValue(info.ValorDesembolso);
            this.financiera.controls['fechaDesembolsoApoyos'].setValue(info.FechaDesembolso);
          } else if (info.TipoFinanciero_idTipoFinanciero === 4) {
            this.financiera.controls['cualConvenios'].setValue(info.Cual);
            this.financiera.controls['valorDesembolsoConvenios'].setValue(info.ValorDesembolso);
            this.financiera.controls['fechaDesembolsoConvenios'].setValue(info.FechaDesembolso);
          }
        }
      }, err => {
        console.log(err);
      });
    }
  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (!this.financiera.invalid) {
      if (this.cookie.check('idIdentUser')) {
        console.log(this.financiera.value);
        this.params = {
          'idFinanciero': 0,
          'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionCredito,
          'Cual': this.financiera.value.cualCredito,
          'ValorDesembolso': this.financiera.value.valorDesembolsoCredito,
          'Tasa': this.financiera.value.tasaCredito,
          'FechaDesembolso': this.financiera.value.fechaDesembolsoCredito,
          'TipoFinanciero_idTipoFinanciero': 1,
          'Socioeconomica_idSocioeconomica': this.data.documento
        };
        this.serviceAsociadoService.registroFinanciero(this.params).subscribe(data => {
          this.params = {
            'idFinanciero': 0,
            'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionahorro,
            'Cual': this.financiera.value.cualahorro,
            'ValorDesembolso': this.financiera.value.valorDesembolsoahorro,
            'Tasa': this.financiera.value.tasaahorro,
            'FechaDesembolso': this.financiera.value.fechaDesembolsoahorro,
            'TipoFinanciero_idTipoFinanciero': 2,
            'Socioeconomica_idSocioeconomica': this.data.documento
          };
          this.serviceAsociadoService.registroFinanciero(this.params).subscribe(data => {
            this.params = {
              'idFinanciero': 0,
              'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionApoyos,
              'Cual': this.financiera.value.cualApoyos,
              'ValorDesembolso': this.financiera.value.valorDesembolsoApoyos,
              'FechaDesembolso': this.financiera.value.fechaDesembolsoApoyos,
              'TipoFinanciero_idTipoFinanciero': 3,
              'Socioeconomica_idSocioeconomica': this.data.documento
            };
            this.serviceAsociadoService.registroFinanciero(this.params).subscribe(data => {
              this.params = {
                'idFinanciero': 0,
                'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionConvenios,
                'Cual': this.financiera.value.cualConvenios,
                'ValorDesembolso': this.financiera.value.valorDesembolsoConvenios,
                'FechaDesembolso': this.financiera.value.fechaDesembolsoConvenios,
                'TipoFinanciero_idTipoFinanciero': 4,
                'Socioeconomica_idSocioeconomica': this.data.documento
              };
              this.serviceAsociadoService.registroFinanciero(this.params).subscribe(data => {
                this.router.navigate(['/educacion']);
              }, err => {
                console.warn(err, 'No se puede registrar');
              })
            }, err => {
              console.warn(err, 'No se puede registrar');
            })
          }, err => {
            console.warn(err, 'No se puede registrar');
          });
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      } else {
        console.log(this.financiera.value);
        this.params = {
          'idFinanciero': this.informacion[0].idFinanciero,
          'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionCredito,
          'Cual': this.financiera.value.cualCredito,
          'ValorDesembolso': this.financiera.value.valorDesembolsoCredito,
          'Tasa': this.financiera.value.tasaCredito,
          'FechaDesembolso': this.financiera.value.fechaDesembolsoCredito,
          'TipoFinanciero_idTipoFinanciero': 1,
          'Socioeconomica_idSocioeconomica': this.data.documento
        };
        this.serviceAsociadoService.actualizarFinanciero(this.params).subscribe(data => {
          this.params = {
            'idFinanciero': this.informacion[1].idFinanciero,
            'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionahorro,
            'Cual': this.financiera.value.cualahorro,
            'ValorDesembolso': this.financiera.value.valorDesembolsoahorro,
            'Tasa': this.financiera.value.tasaahorro,
            'FechaDesembolso': this.financiera.value.fechaDesembolsoahorro,
            'TipoFinanciero_idTipoFinanciero': 2,
            'Socioeconomica_idSocioeconomica': this.data.documento
          };
          this.serviceAsociadoService.actualizarFinanciero(this.params).subscribe(data => {
            this.params = {
              'idFinanciero': this.informacion[2].idFinanciero,
              'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionApoyos,
              'Cual': this.financiera.value.cualApoyos,
              'ValorDesembolso': this.financiera.value.valorDesembolsoApoyos,
              'FechaDesembolso': this.financiera.value.fechaDesembolsoApoyos,
              'TipoFinanciero_idTipoFinanciero': 3,
              'Socioeconomica_idSocioeconomica': this.data.documento
            };
            this.serviceAsociadoService.actualizarFinanciero(this.params).subscribe(data => {
              this.params = {
                'idFinanciero': this.informacion[3].idFinanciero,
                'TipoFinancieroCreditos_idTipoFinancieroCreditos': this.financiera.value.utilizacionConvenios,
                'Cual': this.financiera.value.cualConvenios,
                'ValorDesembolso': this.financiera.value.valorDesembolsoConvenios,
                'FechaDesembolso': this.financiera.value.fechaDesembolsoConvenios,
                'TipoFinanciero_idTipoFinanciero': 4,
                'Socioeconomica_idSocioeconomica': this.data.documento
              };
              this.serviceAsociadoService.actualizarFinanciero(this.params).subscribe(data => {
                this.router.navigate(['/educacion']);
              }, err => {
                console.warn(err, 'No se puede actualizar');
              })
            }, err => {
              console.warn(err, 'No se puede actualizar');
            })
          }, err => {
            console.warn(err, 'No se puede actualizar');
          });
        }, err => {
          console.warn(err, 'No se puede actualizar');
        });
      }
    }
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
