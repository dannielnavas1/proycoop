import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-educacion',
  templateUrl: './educacion.component.html',
  styleUrls: ['./educacion.component.less']
})
export class EducacionComponent implements OnInit {
  educacion: FormGroup;
  params: any;
  data: any;
  viewEdit: boolean;
  dataPersona: any;
  searchData: boolean;
  informacion: any;
  update: boolean;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
              private serviceAsociadoService: ServiceAsociadoService,
              private cookie: CookieService,
              private router: Router) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
                console.log(this.dataInfo)
                this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
              }

  ngOnInit() {
    this.educacion = this.fb.group({
      fecha: ['', [Validators.required]],
      fechaFormal: ['', [Validators.required]],
      tipoDocumentoFormal: ['', [Validators.required]],
      numeroDocumentoFormal: ['', [Validators.required]],
      fechaNoFormal: ['', [Validators.required]],
      tipoDocumentoNoFormal: ['', [Validators.required]],
      numeroDocumentoNoFormal: ['', [Validators.required]],
      fechaBeneficiario: ['', [Validators.required]],
      tipoDocumentoBeneficiario: ['', [Validators.required]],
      numeroDocumentoBeneficiario: ['', [Validators.required]],
      fechaOtros: ['', [Validators.required]],
      tipoDocumentoOtros: ['', [Validators.required]],
      numeroDocumentoOtros: ['', [Validators.required]],
      cual: ['', [Validators.required]],
      fechaOtrosEmprendimiento: ['', [Validators.required]],
      tipoDocumentoOtrosEmprendimiento: ['', [Validators.required]],
      numeroDocumentoOtrosEmprendimiento: ['', [Validators.required]]
    });
    this.searchData = this.cookie.check('idIdentUser');
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
        this.dataPersona = JSON.parse(atob(this.cookie.get('basico')));
      }
    } else {
      this.serviceAsociadoService.consultarDatoscursosEducaciones(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          if (info.TipoEducacion_idTipoEducacion === 7) {
            this.educacion.controls['fecha'].setValue(info.FechaEducacion);
          } else if (info.TipoEducacion_idTipoEducacion === 8) {
            this.educacion.controls['fechaFormal'].setValue(info.FechaEducacion);
            this.educacion.controls['numeroDocumentoFormal'].setValue(info.NumeroIdentificacion);
          } else if (info.TipoEducacion_idTipoEducacion === 9) {
            this.educacion.controls['fechaNoFormal'].setValue(info.FechaEducacion);
            this.educacion.controls['numeroDocumentoNoFormal'].setValue(info.NumeroIdentificacion);
          } else if (info.TipoEducacion_idTipoEducacion === 10) {
            this.educacion.controls['fechaBeneficiario'].setValue(info.FechaEducacion);
            this.educacion.controls['numeroDocumentoBeneficiario'].setValue(info.NumeroIdentificacion);
          } else if (info.TipoEducacion_idTipoEducacion === 11) {
            this.educacion.controls['fechaOtros'].setValue(info.FechaEducacion);
            this.educacion.controls['numeroDocumentoOtros'].setValue(info.NumeroIdentificacion);
            this.educacion.controls['cual'].setValue(info.Cual);
          } else if (info.TipoEducacion_idTipoEducacion === 13) {
            this.educacion.controls['fechaOtrosEmprendimiento'].setValue(info.FechaEducacion);
            this.educacion.controls['numeroDocumentoOtrosEmprendimiento'].setValue(info.NumeroIdentificacion);
          }
        }
      }, err => {
        console.log(err);
      });
    }
  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (!this.educacion.invalid) {
      if (this.cookie.check('idIdentUser')) {
        this.params = {
          'idEducacion': 0,
          'FechaEducacion': this.educacion.value.fecha,
          'TipoEducacion_idTipoEducacion': 7,
          'Socioeconomica_idSocioeconomica': this.data.documento
        };
        this.serviceAsociadoService.registroEducativo(this.params).subscribe(data => {
          this.params = {
            'idEducacion': 0,
            'FechaEducacion': this.educacion.value.fechaFormal,
            'NumeroIdentificacion': this.educacion.value.numeroDocumentoFormal,
            'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoFormal,
            'TipoEducacion_idTipoEducacion': 8,
            'Socioeconomica_idSocioeconomica': this.data.documento
          };
          this.serviceAsociadoService.registroEducativo(this.params).subscribe(data => {
            this.params = {
              'idEducacion': 0,
              'FechaEducacion': this.educacion.value.fechaNoFormal,
              'NumeroIdentificacion': this.educacion.value.numeroDocumentoNoFormal,
              'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoNoFormal,
              'TipoEducacion_idTipoEducacion': 9,
              'Socioeconomica_idSocioeconomica': this.data.documento
            };
            this.serviceAsociadoService.registroEducativo(this.params).subscribe(data => {
              this.params = {
                'idEducacion': 0,
                'FechaEducacion': this.educacion.value.fechaBeneficiario,
                'NumeroIdentificacion': this.educacion.value.numeroDocumentoBeneficiario,
                'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoBeneficiario,
                'TipoEducacion_idTipoEducacion': 10,
                'Socioeconomica_idSocioeconomica': this.data.documento
              };
              this.serviceAsociadoService.registroEducativo(this.params).subscribe(data => {
                this.params = {
                  'idEducacion': 0,
                  'FechaEducacion': this.educacion.value.fechaOtros,
                  'Cual': this.educacion.value.cual,
                  'NumeroIdentificacion': this.educacion.value.numeroDocumentoOtros,
                  'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoOtros,
                  'TipoEducacion_idTipoEducacion': 11,
                  'Socioeconomica_idSocioeconomica': this.data.documento
                };
                this.serviceAsociadoService.registroEducativo(this.params).subscribe(data => {
                    this.params = {
                      'idEducacion': 0,
                      'FechaEducacion': this.educacion.value.fechaOtrosEmprendimiento,
                      'NumeroIdentificacion': this.educacion.value.numeroDocumentoOtrosEmprendimiento,
                      'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoOtrosEmprendimiento,
                      'TipoEducacion_idTipoEducacion': 13,
                      'Socioeconomica_idSocioeconomica': this.data.documento
                    };
                    this.serviceAsociadoService.registroEducativo(this.params).subscribe(data => {
                      this.router.navigate(['/panel']);
                    }, err => {
                      console.warn(err, 'No se puede registrar');
                    });
                  }, err => {
                    console.warn(err, 'No se puede registrar');
                  });
              }, err => {
                console.warn(err, 'No se puede registrar');
              });
            }, err => {
              console.warn(err, 'No se puede registrar');
            });
          }, err => {
            console.warn(err, 'No se puede registrar');
          });
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      } else {
        this.params = {
          'idEducacion': this.informacion[0].idEducacion,
          'FechaEducacion': this.educacion.value.fecha,
          'TipoEducacion_idTipoEducacion': 7,
          'Socioeconomica_idSocioeconomica': this.data.documento
        };
        this.serviceAsociadoService.actualizarEducativo(this.params).subscribe(data => {
          this.params = {
            'idEducacion': this.informacion[0].idEducacion,
            'FechaEducacion': this.educacion.value.fechaFormal,
            'NumeroIdentificacion': this.educacion.value.numeroDocumentoFormal,
            'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoFormal,
            'TipoEducacion_idTipoEducacion': 8,
            'Socioeconomica_idSocioeconomica': this.data.documento
          };
          this.serviceAsociadoService.actualizarEducativo(this.params).subscribe(data => {
            this.params = {
              'idEducacion': this.informacion[1].idEducacion,
              'FechaEducacion': this.educacion.value.fechaNoFormal,
              'NumeroIdentificacion': this.educacion.value.numeroDocumentoNoFormal,
              'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoNoFormal,
              'TipoEducacion_idTipoEducacion': 9,
              'Socioeconomica_idSocioeconomica': this.data.documento
            };
            this.serviceAsociadoService.actualizarEducativo(this.params).subscribe(data => {
              this.params = {
                'idEducacion': this.informacion[2].idEducacion,
                'FechaEducacion': this.educacion.value.fechaBeneficiario,
                'NumeroIdentificacion': this.educacion.value.numeroDocumentoBeneficiario,
                'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoBeneficiario,
                'TipoEducacion_idTipoEducacion': 10,
                'Socioeconomica_idSocioeconomica': this.data.documento
              };
              this.serviceAsociadoService.actualizarEducativo(this.params).subscribe(data => {
                this.params = {
                  'idEducacion': this.informacion[3].idEducacion,
                  'FechaEducacion': this.educacion.value.fechaOtros,
                  'Cual': this.educacion.value.cual,
                  'NumeroIdentificacion': this.educacion.value.numeroDocumentoOtros,
                  'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoOtros,
                  'TipoEducacion_idTipoEducacion': 11,
                  'Socioeconomica_idSocioeconomica': this.data.documento
                };
                this.serviceAsociadoService.actualizarEducativo(this.params).subscribe(data => {
                    this.params = {
                      'idEducacion': this.informacion[4].idEducacion,
                      'FechaEducacion': this.educacion.value.fechaOtrosEmprendimiento,
                      'NumeroIdentificacion': this.educacion.value.numeroDocumentoOtrosEmprendimiento,
                      'Tipocodumento_idTipocodumento': this.educacion.value.tipoDocumentoOtrosEmprendimiento,
                      'TipoEducacion_idTipoEducacion': 13,
                      'Socioeconomica_idSocioeconomica': this.data.documento
                    };
                    this.serviceAsociadoService.actualizarEducativo(this.params).subscribe(data => {
                      this.router.navigate(['/panel']);
                    }, err => {
                      console.warn(err, 'No se puede actualizar');
                    });
                  }, err => {
                    console.warn(err, 'No se puede actualizar');
                  });
              }, err => {
                console.warn(err, 'No se puede actualizar');
              });
            }, err => {
              console.warn(err, 'No se puede actualizar');
            });
          }, err => {
            console.warn(err, 'No se puede actualizar');
          });
        }, err => {
          console.warn(err, 'No se puede actualizar');
        });
      }
    }
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
