import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/service/datos/data.service';

@Component({
  selector: 'app-asociado',
  templateUrl: './asociado.component.html',
  styleUrls: ['./asociado.component.less']
})
export class AsociadoComponent implements OnInit {
  asociado: FormGroup;
  empleo: FormGroup;
  data: any;
  departamentos: any;
  paises: any;
  ciudades: any;
  filtrado: any;
  verCiudad: boolean = false;
  delega: boolean = false;
  delegaDos: boolean = false;
  almacenEmpleados: string[] = [];
  verResumen: boolean = false;
  panelOpenState = false;
  searchData: boolean;
  viewEdit: boolean;
  informacion: any;
  update: boolean;
  infoAsambleas: any;
  infoCooperativos: any;
  infoIntereses: any;
  infoVinculosCoop: any;
  viewEdits: boolean;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
    private serviceAsociadoService: ServiceAsociadoService,
    private cookie: CookieService,
    private dataService: DataService,
    private router: Router) {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    console.log(this.data)
    this.data.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
    this.dataService.departamento().subscribe(data => this.departamentos = data);
    this.dataService.pais().subscribe(data => this.paises = data);
  }

  ngOnInit() {
    this.empleo = this.fb.group({
      nit: ['', [Validators.required]],
      empresa: ['', [Validators.required]],
      cargo: ['', [Validators.required]],
      salario: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      ciudad: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
      tipoContrato: ['', [Validators.required]],
      fechaInicio: ['', [Validators.required]],
      fechaFinal: [''],
      tiempoAnhos: ['', [Validators.required]],
      tiempoMeses: ['', [Validators.required]],
      porcentajeDescuentoBase: ['', [Validators.required]],
      porcentajeDescuentoAdicional: ['', [Validators.required]],
    });
    this.asociado = this.fb.group({
      curso: ['', [Validators.required]],
      fechaCooperativos: ['', [Validators.required]],
      poseeVehiculo: ['', [Validators.required]],
      otrosIngresos: ['', [Validators.required]],
      fechaOrdinaria: ['', [Validators.required]],
      asistio: ['', [Validators.required]],
      // delego: [''],
      tipoDocumento: [''],
      numeroDocumento: [''],
      fechaExtraordinaria: ['', [Validators.required]],
      asistioExtraordinario: ['', [Validators.required]],
      // delegoExtraordinario: [''],
      tipoDocumentoExtraordinario: [''],
      numeroDocumentoExtraordinario: [''],
      estado: ['', [Validators.required]],
      fechaAfiliacion: ['', [Validators.required]],
      fechaRetiro: ['', [Validators.required]],
      fechaEstado: ['', [Validators.required]],
      causal: ['', [Validators.required]]
    });
    this.searchData = this.cookie.check('idIdentUser');
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdits = true;
      }
    } else {
      this.serviceAsociadoService.consultarDatosEmpleos(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          // this.natural.controls['primerNombre'].setValue(info.PrimerNombre);
        }
        this.serviceAsociadoService.consultarDatoscursosAsambleas(atob(this.cookie.get('idIdentUser')))
          .subscribe(data => {
            this.infoAsambleas = data;
            console.log(this.infoAsambleas);
          });
        this.serviceAsociadoService.consultarDatoscursosCooperativos(atob(this.cookie.get('idIdentUser')))
          .subscribe(data => {
            this.infoCooperativos = data;
            console.log(this.infoCooperativos);
          });
        this.serviceAsociadoService.consultarDatoscursosInteres(atob(this.cookie.get('idIdentUser')))
          .subscribe(data => {
            this.infoIntereses = data;
            console.log(this.infoIntereses);
          });
        this.serviceAsociadoService.consultarDatoscursosVinculoscooperativos(atob(this.cookie.get('idIdentUser')))
          .subscribe(data => {
            this.infoVinculosCoop = data;
            console.log(this.infoVinculosCoop);
          });
      }, err => {
        console.log(err);
      });
    }
  }

  onSubmit() {
    console.log(this.asociado.value);
    if (!this.asociado.invalid) {
      this.data = JSON.parse(atob(this.cookie.get('basico')));
      this.serviceAsociadoService.registroAsociadoAsistenciaAsamblea(this.asociado.value, this.data.documento).subscribe(data => {
        this.serviceAsociadoService.registroAsociadoAsistenciaAsambleaDos(this.asociado.value, this.data.documento).subscribe(data => {
          this.serviceAsociadoService.registroAsociadoCursoCoop(this.asociado.value, this.data.documento).subscribe(data => {
            for (let almacen of this.almacenEmpleados) {
              this.serviceAsociadoService.registroAsociadoEmpleo(almacen, this.data.documento)
                .subscribe(data => console.log('empleo'), err => {
                console.warn(err, 'No se puede registrar');
              });
            }
            this.serviceAsociadoService.registroAsociadoInteres(this.asociado.value, this.data.documento).subscribe(data => {
              this.serviceAsociadoService.registroAsociadoVinculoCoop(this.asociado.value, this.data.documento).subscribe(data => {
                this.router.navigate(['/financiero']);
              }, err => {
                console.warn(err, 'No se puede registrar');
              });
            }, err => {
              console.warn(err, 'No se puede registrar');
            });

          }, err => {
            console.warn(err, 'No se puede registrar');
          });
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      }, err => {
        console.warn(err, 'No se puede registrar');
      });
    } else {
      alert('No entra');
    }
  }

  onSubmitEmpleado() {
    this.almacenEmpleados.push(this.empleo.value);
    this.verResumen = true;
    console.log(this.almacenEmpleados);
    this.empleo.reset();
  }

  deleteBeneficiario(i) {
    this.almacenEmpleados.splice(i, 1);
    console.log(this.almacenEmpleados);
    if (this.almacenEmpleados.length === 0) {
      this.verResumen = false;
    }
  }


  departamentoCiudad() {
    if (this.empleo.value.departamento !== '') {
      this.filtrado = this.departamentos.filter(v => {
        return v.idDepartamentos === this.empleo.value.departamento;
      });
      for (const c of this.filtrado) {
        this.dataService.ciudad(c.Departamento).subscribe(data => this.ciudades = data);
        this.verCiudad = true;
      }
    }
  }

  activarAsistencia(event) {
    console.log(event.value);
    if (event.value !== '0') {
      console.log('Mostrar datos');
      this.delega = false;
    } else {
      console.log('Mostrar otros datos');
      this.delega = true;
    }
  }

  activarAsistenciaDos(event) {
    console.log(event.value);
    if (event.value !== '0') {
      console.log('Mostrar datos');
      this.delegaDos = false;
    } else {
      console.log('Mostrar otros datos');
      this.delegaDos = true;
    }
  }

  editForm() {
    this.viewEdits = true;
  }

  closeEdit() {
    this.viewEdits = false;
  }

}
