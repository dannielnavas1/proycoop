import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonacontactoComponent } from './personacontacto.component';

describe('PersonacontactoComponent', () => {
  let component: PersonacontactoComponent;
  let fixture: ComponentFixture<PersonacontactoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonacontactoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonacontactoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
