import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/service/datos/data.service';

@Component({
  selector: 'app-personacontacto',
  templateUrl: './personacontacto.component.html',
  styleUrls: ['./personacontacto.component.less']
})
export class PersonacontactoComponent implements OnInit {
  contacto: FormGroup;
  data: any;
  departamentos: any;
  paises: any;
  ciudades: any;
  filtrado: any;
  verMunicipio: boolean = false;
  verCiudad: boolean = false;
  viewEdit: boolean;
  searchData: boolean;
  informacion: any;
  update: boolean;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
              private serviceAsociadoService: ServiceAsociadoService,
              private cookie: CookieService,
              private dataService: DataService,
              private router: Router) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
                console.log(this.dataInfo)
                this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
                this.dataService.departamento().subscribe(data => this.departamentos = data);
                this.dataService.pais().subscribe(data => this.paises = data);
              }

  ngOnInit() {
    this.contacto = this.fb.group({
      tipoDocumento: ['', [Validators.required]],
      numeroDocumento: ['', [Validators.required]],
      primerNombre: ['', [Validators.required]],
      segundoNombre: [''],
      primerApellido: ['', [Validators.required]],
      segundoApellido: [''],
      telefono: [''],
      celular: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      area: ['', [Validators.required]],
      barrio: ['', [Validators.required]],
      ciudad: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
      pais: ['', [Validators.required]],
    });

    this.searchData = this.cookie.check('idIdentUser');
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
      }
    } else {
      this.serviceAsociadoService.consultarDatosContactos(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          this.contacto.controls['numeroDocumento'].setValue(info.idContacto);
          this.contacto.controls['primerNombre'].setValue(info.PrimerNombre);
          this.contacto.controls['segundoNombre'].setValue(info.SegundoNombre);
          this.contacto.controls['primerApellido'].setValue(info.PrimerApellido);
          this.contacto.controls['segundoApellido'].setValue(info.SegundoApellido);
          this.contacto.controls['telefono'].setValue(info.Telefono);
          this.contacto.controls['celular'].setValue(info.Celular);
          this.contacto.controls['direccion'].setValue(info.Direccion);
          this.contacto.controls['barrio'].setValue(info.Barrio);
        }
      }, err => {
        console.log(err);
      });
    }
  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (!this.contacto.invalid) {
      if (this.cookie.check('idIdentUser')) {
        this.serviceAsociadoService.registroContacto(this.contacto.value, this.data.documento).subscribe(data => {
          this.router.navigate(['/asociado']);
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      } else {
        this.serviceAsociadoService
        .actualizarContacto(this.contacto.value, this.data.documento, this.informacion.idContacto)
          .subscribe(data => {
            console.log(data);
            this.router.navigate(['/asociado']);
          }, err => console.log(err));
      }
    }
  }

  ifColombia() {
    if (this.contacto.value.pais === 169) {
      this.verMunicipio = true;
    } else {
      this.verMunicipio = false;
    }
  }

  departamentoCiudad() {
    if (this.contacto.value.departamento !== '') {
      this.filtrado = this.departamentos.filter(v => {
        return v.idDepartamentos === this.contacto.value.departamento;
      });
      for (const c of this.filtrado) {
        this.dataService.ciudad(c.Departamento).subscribe(data => this.ciudades = data);
        this.verCiudad = true;
      }
    }
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
