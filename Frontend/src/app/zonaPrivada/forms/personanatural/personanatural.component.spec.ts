import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonanaturalComponent } from './personanatural.component';

describe('PersonanaturalComponent', () => {
  let component: PersonanaturalComponent;
  let fixture: ComponentFixture<PersonanaturalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonanaturalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonanaturalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
