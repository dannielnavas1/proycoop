import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/datos/data.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-personanatural',
  templateUrl: './personanatural.component.html',
  styleUrls: ['./personanatural.component.less']
})
export class PersonanaturalComponent implements OnInit {
  natural: FormGroup;
  data: any;
  dataPersona: any;
  dataNombre: any;
  primerNombres: any;
  segundoNombre: any;
  primerApellido: any;
  segundoApellido: any;
  informacion: any;
  update: boolean;
  searchData: boolean;
  viewEdit: boolean = false;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
              private serviceAsociadoService: ServiceAsociadoService,
              private dataService: DataService,
              private cookie: CookieService,
              private router: Router) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
                console.log(this.dataInfo)
                this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
              }

  ngOnInit() {
    this.natural = this.fb.group({
      primerNombre: ['', [Validators.required]],
      segundoNombre: [''],
      primerApellido: ['', [Validators.required]],
      segundoApellido: [''],
      fechaNacimiento: ['', [Validators.required]],
      genero: ['', [Validators.required]],
      etnia: ['', [Validators.required]],
      nivelEducativo: ['', [Validators.required]],
      estadoCivil: ['', [Validators.required]],
      estrato: ['', [Validators.required]],
      seguridadSocial: ['', [Validators.required]],
      tipoVivienda: ['', [Validators.required]],
      cabezaFamilia: ['', [Validators.required]],
      personasAcargo: ['', [Validators.required]],
      actividadAsociado: ['', [Validators.required]]
    });
    this.searchData = this.cookie.check('idIdentUser');
    if (!this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
        this.dataPersona = JSON.parse(atob(this.cookie.get('basico')));
        this.dataNombre = this.dataPersona.nombreRazon.split(' ');
        this.natural.controls['primerNombre'].setValue(this.dataNombre[0]);
        this.natural.controls['segundoNombre'].setValue(this.dataNombre[1]);
        this.natural.controls['primerApellido'].setValue(this.dataNombre[2]);
        this.natural.controls['segundoApellido'].setValue(this.dataNombre[3]);
        this.data = JSON.parse(atob(this.cookie.get('basico')));
      }
    } else {
      this.serviceAsociadoService.consultarDatos(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          this.natural.controls['primerNombre'].setValue(info.PrimerNombre);
          this.natural.controls['segundoNombre'].setValue(info.SegundoNombre);
          this.natural.controls['primerApellido'].setValue(info.PrimerApellido);
          this.natural.controls['segundoApellido'].setValue(info.SegundoApellido);
          this.natural.controls['fechaNacimiento'].setValue(info.FechaNacimiento);
          this.natural.controls['personasAcargo'].setValue(info.PersonasAcargo);
        }
      }, err => {
        console.log(err);
      });
    }
  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (!this.natural.invalid) {
      if (!this.cookie.check('idIdentUser')) {
        this.serviceAsociadoService.registroPersonaNatural(this.natural.value, this.data.documento)
        .subscribe(data => {
          this.router.navigate(['/ubicacion']);
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      } else {
        this.serviceAsociadoService
          .actualizarPersonaNatural(this.natural.value, this.data.documento, this.informacion.idPersona)
            .subscribe(data => {
              console.log(data);
              this.router.navigate(['/ubicacion']);
            }, err => console.log(err));
      }
    }
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
