import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardInfoAsociadoComponent } from './card-info-asociado.component';

describe('CardInfoAsociadoComponent', () => {
  let component: CardInfoAsociadoComponent;
  let fixture: ComponentFixture<CardInfoAsociadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardInfoAsociadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardInfoAsociadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
