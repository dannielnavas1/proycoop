import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-card-info-asociado',
  templateUrl: './card-info-asociado.component.html',
  styleUrls: ['./card-info-asociado.component.less']
})
export class CardInfoAsociadoComponent implements OnInit {
  dataInfo: any;

  constructor(private cookie: CookieService) {
    this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
  }

  ngOnInit() {
  }

}
