import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/service/datos/data.service';

@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.component.html',
  styleUrls: ['./ubicacion.component.less']
})
export class UbicacionComponent implements OnInit {
  ubicacion: FormGroup;
  data: any;
  departamentos: any;
  paises: any;
  ciudades: any;
  filtrado: any;
  verMunicipio: boolean = false;
  verCiudad: boolean = false;
  searchData: boolean;
  viewEdit: boolean;
  dataPersona: any;
  informacion: any;
  update: boolean;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
              private serviceAsociadoService: ServiceAsociadoService,
              private cookie: CookieService,
              private dataService: DataService,
              private router: Router) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
                console.log(this.dataInfo)
                this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
                this.dataService.departamento().subscribe(data => this.departamentos = data);
                this.dataService.pais().subscribe(data => this.paises = data);
              }

  ngOnInit() {
    this.ubicacion = this.fb.group({
      telefono: [''],
      celular: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.email]],
      direccion: ['', [Validators.required]],
      area: ['', [Validators.required]],
      barrio: ['', [Validators.required]],
      ciudad: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
      pais: ['', [Validators.required]],
    });

    this.searchData = this.cookie.check('idIdentUser');
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
        // this.dataPersona = JSON.parse(atob(this.cookie.get('basico')));
        // this.data = JSON.parse(atob(this.cookie.get('basico')));
      }
    } else {
      this.serviceAsociadoService.consultarDatosUbicaciones(atob(this.cookie.get('idIdentUser'))).subscribe(data => {
        this.informacion = data;
        console.log(this.informacion);
        this.update = true;
        for (let info of this.informacion) {
          this.ubicacion.controls['telefono'].setValue(info.Telefono);
          this.ubicacion.controls['celular'].setValue(info.Celular);
          this.ubicacion.controls['correo'].setValue(info.Correo);
          this.ubicacion.controls['direccion'].setValue(info.Direccion);
          this.ubicacion.controls['barrio'].setValue(info.Barrio);
        }
      }, err => {
        console.log(err);
      });
    }

  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (!this.ubicacion.invalid) {
      if (this.cookie.check('idIdentUser')) {
        this.serviceAsociadoService.registroUbicacion(this.ubicacion.value, this.data.documento).subscribe(data => {
          this.router.navigate(['/beneficiarios']);
        }, err => {
          console.warn(err, 'No se puede registrar');
          alert('No podemos procesar su solicitud, por favor comuniquese con el administrador.');
        });
      } else {
        this.serviceAsociadoService
          .actualizarUbicacion(this.ubicacion.value, this.data.documento, this.informacion.idUbicacion)
            .subscribe(data => {
              console.log(data);
              this.router.navigate(['/ubicacion']);
            }, err => console.log(err));
      }
    }
  }

  ifColombia() {
    if (this.ubicacion.value.pais === 169) {
      this.verMunicipio = true;
    } else {
      this.verMunicipio = false;
    }
  }

  departamentoCiudad() {
    if (this.ubicacion.value.departamento !== '') {
      this.filtrado = this.departamentos.filter(v => {
        return v.idDepartamentos === this.ubicacion.value.departamento;
      });
      for (const c of this.filtrado) {
        this.dataService.ciudad(c.Departamento).subscribe(data => this.ciudades = data);
        this.verCiudad = true;
      }
    }
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
