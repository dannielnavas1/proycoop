import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceAsociadoService } from 'src/app/setvice/asociado/service-asociado.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-personajuridica',
  templateUrl: './personajuridica.component.html',
  styleUrls: ['./personajuridica.component.less']
})
export class PersonajuridicaComponent implements OnInit {
  juridica: FormGroup;
  data: any;
  searchData: boolean;
  viewEdit: boolean;
  dataPersona: any;
  informacion: any;
  update: boolean;
  representante: any;
  dataInfo: any;
  viewSpecial: boolean;

  constructor(private fb: FormBuilder,
    private serviceAsociadoService: ServiceAsociadoService,
    private cookie: CookieService,
    private router: Router) { 
      this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
      console.log(this.dataInfo)
      this.dataInfo.tipoDocumento === 1 ? this.viewSpecial = true : this.viewSpecial = false
    }

  ngOnInit() {
    this.juridica = this.fb.group({
      razonSocial: ['', [Validators.required]],
      fechaAfiliación: ['', [Validators.required]],
      tipoDerecho: ['', [Validators.required]],
      actividad: [''],
      valorBase: ['', [Validators.required]],
      primerNombre: ['', [Validators.required]],
      segundoNombre: ['', [Validators.required]],
      primerApellido: ['', [Validators.required]],
      segundoApellido: [''],
      tipoDocumento: ['', [Validators.required]],
      numeroDocumento: ['', [Validators.required]],
      telefono: [''],
      celular: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.email]]
    });

    this.searchData = this.cookie.check('idIdentUser');
    console.log(this.searchData)
    if (this.cookie.check('idIdentUser')) {
      if (this.cookie.check('basico')) {
        this.viewEdit = true;
        this.dataPersona = JSON.parse(atob(this.cookie.get('basico')));
        this.juridica.controls['razonSocial'].setValue(this.dataPersona.nombreRazon);
        this.data = JSON.parse(atob(this.cookie.get('basico')));
      }
    } else {
      this.serviceAsociadoService.consultarDatosJuridica(atob(this.cookie.get('idIdentUser')))
        .subscribe(data => {
          this.informacion = data;
          console.log(this.informacion);
          this.update = true;
          for (let info of this.informacion) {
            this.juridica.controls['razonSocial'].setValue(info.RazonSocial);
            this.juridica.controls['fechaAfiliación'].setValue(info.FechaAfiliacion);
            this.juridica.controls['valorBase'].setValue(info.ValorBase);
          }
          this.serviceAsociadoService.consultarDatosRepredentante(atob(this.cookie.get('idIdentUser')))
            .subscribe(data => {
              this.representante = data;
              console.log(this.representante);
              for (let rep of this.representante) {
                this.juridica.controls['primerNombre'].setValue(rep.PrimerNombre);
                this.juridica.controls['segundoNombre'].setValue(rep.SegundoNombre);
                this.juridica.controls['primerApellido'].setValue(rep.PrimerApellido);
                this.juridica.controls['segundoApellido'].setValue(rep.SegundoApellido);
                this.juridica.controls['numeroDocumento'].setValue(rep.idRepresentanteLegal);
                this.juridica.controls['telefono'].setValue(rep.Telefono);
                this.juridica.controls['celular'].setValue(rep.Celular);
                this.juridica.controls['correo'].setValue(rep.Correo);
              }
            }, err => console.log(err));
        }, err => console.log(err));
    }
  }

  onSubmit() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    if (!this.juridica.invalid) {
    if (this.cookie.check('idIdentUser')) {
      this.serviceAsociadoService.registroPersonaJuridica(this.juridica.value, this.data.documento).subscribe(data => {
        this.serviceAsociadoService.registroRepresentante(this.juridica.value, this.data.documento).subscribe(data => {
          this.router.navigate(['/ubicacion']);
        }, err => {
          console.warn(err, 'No se puede registrar');
        });
      }, err => {
        console.warn(err, 'No se puede registrar');
      });
    } else {
      this.serviceAsociadoService
        .actualizarPersonaJuridica(this.juridica.value, this.data.documento, this.informacion.idPersonauridica)
          .subscribe(data => {
            this.serviceAsociadoService
              .actualizarRepresentante(this.juridica.value, this.data.documento, this.representante.idRepresentanteLegal)
                .subscribe(data => {
                  console.log(data);
                });
          }, err => {
            console.log(err);
          });
    }
    }
  }

  editForm() {
    this.viewEdit = true;
  }

  closeEdit() {
    this.viewEdit = false;
  }

}
