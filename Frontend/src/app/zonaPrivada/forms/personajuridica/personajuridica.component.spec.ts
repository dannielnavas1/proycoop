import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajuridicaComponent } from './personajuridica.component';

describe('PersonajuridicaComponent', () => {
  let component: PersonajuridicaComponent;
  let fixture: ComponentFixture<PersonajuridicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonajuridicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonajuridicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
