import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';

@Component({
  selector: 'app-otros',
  templateUrl: './otros.component.html',
  styleUrls: ['./otros.component.less']
})
export class OtrosComponent implements OnInit {
  otros: FormGroup;
  dataInfo: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
              }

  ngOnInit() {
    this.otros = this.fb.group({
      desarrollado: ['', [Validators.required]],
      alianza: ['', [Validators.required]],
      acuerdos: ['', [Validators.required]],
      convenios: ['', [Validators.required]],
      alianzaPrivadas: ['', [Validators.required]],
      acuerdosPrivadas: ['', [Validators.required]],
      conveniosPrivadas: ['', [Validators.required]],
      alianzaInternacionales: ['', [Validators.required]],
      acuerdosInternacionales: ['', [Validators.required]],
      conveniosInternacionales: ['', [Validators.required]],
      publicas: ['', [Validators.required]],
      privadas: ['', [Validators.required]],
      internal: ['', [Validators.required]],
      monton: ['', [Validators.required]],
      beneficiarios: ['', [Validators.required]],
      publicasDesarrollar: ['', [Validators.required]],
      privadasDesarrollar: ['', [Validators.required]],
      internalDesarrollar: ['', [Validators.required]],
    });
  }
  
  onSubmit() {
    if (!this.otros.invalid) {
      this.serviceasociadosService.organizacionOtros(this.otros.value, this.dataInfo.nit).subscribe(data => {
        // TODO: falta enviar los diferentes envios
        this.serviceasociadosService.organizacionRelacionesDos(this.otros.value, this.dataInfo.nit)
          .subscribe(data => {}, err => console.log(err));
        this.serviceasociadosService.organizacionRelacionesTres(this.otros.value, this.dataInfo.nit)
        .subscribe(data => {}, err => console.log(err));
        this.serviceasociadosService.organizacionRelaciones(this.otros.value, this.dataInfo.nit).subscribe(data => {
          this.serviceasociadosService.organizacionObjetivosCompromiso(this.otros.value, this.dataInfo.nit).subscribe(data => {
            this.serviceasociadosService.organizacionAlianzas(this.otros.value, this.dataInfo.nit).subscribe(data => {
              this.serviceasociadosService.organizacionImpactoCompromiso(this.otros.value, this.dataInfo.nit).subscribe(data => {
                this.router.navigate(['/orgevaluaciones']);
              }, err => console.log(err));
            }, err => console.log(err));
          }, err => console.log(err));
        }, err => console.log(err));
      }, err => console.log(err));
    }
  }

}
