import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';

@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.component.html',
  styleUrls: ['./evaluacion.component.less']
})
export class EvaluacionComponent implements OnInit {
  evaluacion: FormGroup;
  dataInfo: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
              }

  ngOnInit() {
    this.evaluacion = this.fb.group({
      evaluacion: ['', [Validators.required]],
      balance: ['', [Validators.required]]
    });
  }

  onSubmit() {
    if (!this.evaluacion.invalid) {
      this.serviceasociadosService.organizacionEvaluacion(this.evaluacion.value, this.dataInfo.nit)
        .subscribe(data => {
          this.router.navigate(['/panel']);
        }, err => console.log(err));
    }
  }

}
