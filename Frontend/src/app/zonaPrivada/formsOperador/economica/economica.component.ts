import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';

@Component({
  selector: 'app-economica',
  templateUrl: './economica.component.html',
  styleUrls: ['./economica.component.less']
})
export class EconomicaComponent implements OnInit {
  economica: FormGroup;
  dataInfo: any;
  idOrg: any;
  dataUser: any;
  viewCard: boolean;
  dataParticipacion: any;
  dataAutonomia: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.viewCard = this.cookie.check('idIdentUser');
                if (this.cookie.check('idIdentUser')) {
                  this.idOrg = atob(this.cookie.get('idIdentUser'));
                  this.serviceasociadosService.getOrganizacionEconomica(this.idOrg)
                    .subscribe(data => {
                      this.dataUser = data;
                      this.serviceasociadosService.getOrganizacionParticipacionEconomica(this.idOrg)
                        .subscribe(data => {
                          this.dataParticipacion = data;
                          this.serviceasociadosService.getOrganizacionAutonomia(this.idOrg)
                            .subscribe(data => {
                              this.dataAutonomia = data;
                              console.log(this.dataAutonomia);
                            }, err => console.log(err));
                        }, err => console.log(err));
                    }, err => console.log(err));
                }
              }

  ngOnInit() {
    this.economica = this.fb.group({
      ahorros: ['', [Validators.required]],
      aportes: ['', [Validators.required]],
      reducibles: ['', [Validators.required]],
      monto: ['', [Validators.required]],
      total: ['', [Validators.required]],
      exedente: ['', [Validators.required]],
      distribucion: ['', [Validators.required]],
      fondoEducativo: ['', [Validators.required]],
      fondoSolidaridad: ['', [Validators.required]],
      fondoBienestar: ['', [Validators.required]],
      fondoDesarrollo: ['', [Validators.required]],
      otroFondo: ['', [Validators.required]],
      cual: ['', [Validators.required]],
      valorApalancamientoPropio: ['', [Validators.required]],
      tasaApalancamientoPropio: ['', [Validators.required]],
      valorApalancamientoTerceros: ['', [Validators.required]],
      tasaApalancamientoTerceros: ['', [Validators.required]],
      patrocinio: ['', [Validators.required]],
      gobierno: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
    console.log(this.dataInfo);
    if (!this.economica.invalid) {
      if (this.viewCard) {
        // this.serviceasociadosService.putOrganizacionEconomica(this.economica.value, this.dataInfo.nit).subscribe(data => {
        //   this.serviceasociadosService.putOrganizacionParticipacionEconomica(this.economica.value, this.dataInfo.nit).subscribe(data => {
        //     let json = {
        //       'Valor': this.economica.value.valorApalancamientoPropio,
        //       'Tasa': this.economica.value.tasaApalancamientoPropio,
        //       'CuentaBuenGobierno': null,
        //       'PatrocinioBeneficio': null,
        //       'TipoAutonomia_idTipoAutonomia': 1,
        //       'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': this.dataInfo.nit
        //     };
        //     this.serviceasociadosService.putOrganizacionAutonomia(json, this.idOrg).subscribe(data => {
        //       let json2 = {
        //         'Valor': this.economica.value.valorApalancamientoTerceros,
        //         'Tasa': this.economica.value.tasaApalancamientoPropio,
        //         'CuentaBuenGobierno': null,
        //         'PatrocinioBeneficio': null,
        //         'TipoAutonomia_idTipoAutonomia': 2,
        //         'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': this.dataInfo.nit
        //       };
        //       this.serviceasociadosService.putOrganizacionAutonomia(json2, this.idOrg).subscribe(data => {
        //         console.log(this.economica.value.gobierno)
        //         let json3 = {
        //           'Valor': null,
        //           'Tasa': null,
        //           'CuentaBuenGobierno': this.economica.value.gobierno,
        //           'PatrocinioBeneficio': this.economica.value.patrocinio,
        //           'TipoAutonomia_idTipoAutonomia': 3,
        //           'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': this.dataInfo.nit
        //         };
        //         this.serviceasociadosService.putOrganizacionAutonomia(json3, this.idOrg).subscribe(data => {
        //           this.router.navigate(['/orgeducacion']);
        //         }, err => console.log(err));
        //       }, err => console.log(err));
        //     }, err => console.log(err));
  
        //   }, err => console.log(err));
        // }, err => console.log(err));
      } else {
        this.serviceasociadosService.organizacionEconomica(this.economica.value, this.dataInfo.nit).subscribe(data => {
          this.serviceasociadosService.organizacionParticipacionEconomica(this.economica.value, this.dataInfo.nit).subscribe(data => {
            let json = {
              'Valor': this.economica.value.valorApalancamientoPropio,
              'Tasa': this.economica.value.tasaApalancamientoPropio,
              'CuentaBuenGobierno': null,
              'PatrocinioBeneficio': null,
              'TipoAutonomia_idTipoAutonomia': 1,
              'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': this.dataInfo.nit
            };
            this.serviceasociadosService.organizacionAutonomia(json).subscribe(data => {
              let json2 = {
                'Valor': this.economica.value.valorApalancamientoTerceros,
                'Tasa': this.economica.value.tasaApalancamientoPropio,
                'CuentaBuenGobierno': null,
                'PatrocinioBeneficio': null,
                'TipoAutonomia_idTipoAutonomia': 2,
                'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': this.dataInfo.nit
              };
              this.serviceasociadosService.organizacionAutonomia(json2).subscribe(data => {
                console.log(this.economica.value.gobierno)
                let json3 = {
                  'Valor': null,
                  'Tasa': null,
                  'CuentaBuenGobierno': this.economica.value.gobierno,
                  'PatrocinioBeneficio': this.economica.value.patrocinio,
                  'TipoAutonomia_idTipoAutonomia': 3,
                  'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': this.dataInfo.nit
                };
                this.serviceasociadosService.organizacionAutonomia(json3).subscribe(data => {
                  this.router.navigate(['/orgeducacion']);
                }, err => console.log(err));
              }, err => console.log(err));
            }, err => console.log(err));
  
          }, err => console.log(err));
        }, err => console.log(err));
      }
    }
  }

  editForm() {
    this.viewCard = false;
  }

  closeEdit() {
    this.viewCard = true;
  }

}
