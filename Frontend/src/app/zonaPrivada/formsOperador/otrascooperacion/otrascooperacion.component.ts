import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';
import { DataService } from 'src/app/service/datos/data.service';

@Component({
  selector: 'app-otrascooperacion',
  templateUrl: './otrascooperacion.component.html',
  styleUrls: ['./otrascooperacion.component.less']
})
export class OtrascooperacionComponent implements OnInit {
  otrasCooperaciones: FormGroup;
  dataInfo: any;
  paises: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private dataService: DataService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
                this.dataService.pais().subscribe(data => this.paises = data);
              }

  ngOnInit() {
    this.otrasCooperaciones = this.fb.group({
      economicos: ['', [Validators.required]],
      educativos: ['', [Validators.required]],
      culturales: ['', [Validators.required]],
      tiposAcuerdo: ['', [Validators.required]],
      membresia: ['', [Validators.required]],
      entidad: ['', [Validators.required]],
      acuerdos: ['', [Validators.required]],
      pais: ['', [Validators.required]],
      acuerdo: ['', [Validators.required]],
      objetivo: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.serviceasociadosService.organizacionOtrasCooperaciones(this.otrasCooperaciones.value, this.dataInfo.nit)
      .subscribe(data => {
        this.serviceasociadosService.organizacionOrganizacionalesInter(this.otrasCooperaciones.value, this.dataInfo.nit)
          .subscribe(data => {
            // TODO: falta recorrer los datos de los dos items
            this.serviceasociadosService.organizacionAcuerdos(this.otrasCooperaciones.value, this.dataInfo.nit)
              .subscribe(data => {
                this.serviceasociadosService.organizacionObjetivos(this.otrasCooperaciones.value, this.dataInfo.nit)
                  .subscribe(data => {
                    this.router.navigate(['/orgcomunidades']);
                  }, err => console.log(err));
              }, err => console.log(err));
          }, err => console.log(err));
      }, err => console.log(err));
  }

}
