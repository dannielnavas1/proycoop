import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtrascooperacionComponent } from './otrascooperacion.component';

describe('OtrascooperacionComponent', () => {
  let component: OtrascooperacionComponent;
  let fixture: ComponentFixture<OtrascooperacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtrascooperacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtrascooperacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
