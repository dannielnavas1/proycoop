import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtrascomunidadComponent } from './otrascomunidad.component';

describe('OtrascomunidadComponent', () => {
  let component: OtrascomunidadComponent;
  let fixture: ComponentFixture<OtrascomunidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtrascomunidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtrascomunidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
