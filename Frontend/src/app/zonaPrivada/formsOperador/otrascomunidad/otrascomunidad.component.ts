import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';
import { DataService } from 'src/app/service/datos/data.service';

@Component({
  selector: 'app-otrascomunidad',
  templateUrl: './otrascomunidad.component.html',
  styleUrls: ['./otrascomunidad.component.less']
})
export class OtrascomunidadComponent implements OnInit {
  comunidadOtra: FormGroup;
  dataInfo: any;
  departamentos: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private dataService: DataService,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
                this.dataService.departamento().subscribe(data => this.departamentos = data);
              }

  ngOnInit() {
    this.comunidadOtra = this.fb.group({
      adelanta: ['', [Validators.required]],
      caracteristica: ['', [Validators.required]],
      monto: ['', [Validators.required]],
      beneficiarios: ['', [Validators.required]],
      municipio: ['', [Validators.required]],
      impacto: ['', [Validators.required]],
      realizaProg: ['', [Validators.required]],
      progCulturales: ['', [Validators.required]],
      montoInvertido: ['', [Validators.required]],
      beneficiosCultura: ['', [Validators.required]],
      municipiosCultura: ['', [Validators.required]],
      impactoCultural: ['', [Validators.required]],
      participa: ['', [Validators.required]],
      valor: ['', [Validators.required]]
    });
  }

  onSubmit() {
    if (!this.comunidadOtra.invalid) {
      this.serviceasociadosService.organizacionComunidadAmbiente(this.comunidadOtra.value, this.dataInfo.nit).subscribe(data => {
        this.serviceasociadosService.organizacionCaracteristicasAmbiental(this.comunidadOtra.value, this.dataInfo.nit).subscribe(data => {
          this.serviceasociadosService.organizacionImpactoAmbiental(this.comunidadOtra.value, this.dataInfo.nit).subscribe(data => {
            this.serviceasociadosService.organizacionComunidadDesarrollo(this.comunidadOtra.value, this.dataInfo.nit).subscribe(data => {
              this.serviceasociadosService.organizacionCaracteristicasEcoComun(this.comunidadOtra.value, this.dataInfo.nit)
                .subscribe(data => {
                this.serviceasociadosService.organizacionImpactoEcoComun(this.comunidadOtra.value, this.dataInfo.nit).subscribe(data => {
                  this.serviceasociadosService.organizacionOtrasComunidad(this.comunidadOtra.value, this.dataInfo.nit).subscribe(data => {
                    this.router.navigate(['/orgotros']);
                  }, err => console.log(err));
                }, err => console.log(err));
              }, err => console.log(err));
            }, err => console.log(err));
          }, err => console.log(err));
        }, err => console.log(err));
      }, err => console.log(err));
    }
  }

}
