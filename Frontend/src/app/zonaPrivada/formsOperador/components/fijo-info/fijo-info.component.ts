import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-fijo-info',
  templateUrl: './fijo-info.component.html',
  styleUrls: ['./fijo-info.component.less']
})
export class FijoInfoComponent implements OnInit {
  dataInfo: any;

  constructor(private cookie: CookieService) {
    this.dataInfo = JSON.parse(atob(this.cookie.get('basico')));
  }

  ngOnInit() {
  }

}
