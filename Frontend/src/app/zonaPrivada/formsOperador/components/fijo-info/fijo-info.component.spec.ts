import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FijoInfoComponent } from './fijo-info.component';

describe('FijoInfoComponent', () => {
  let component: FijoInfoComponent;
  let fixture: ComponentFixture<FijoInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FijoInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FijoInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
