import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CooperacionComponent } from './cooperacion.component';

describe('CooperacionComponent', () => {
  let component: CooperacionComponent;
  let fixture: ComponentFixture<CooperacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CooperacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
