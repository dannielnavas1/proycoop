import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';

@Component({
  selector: 'app-cooperacion',
  templateUrl: './cooperacion.component.html',
  styleUrls: ['./cooperacion.component.less']
})
export class CooperacionComponent implements OnInit {
  cooperacion: FormGroup;
  dataInfo: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
              }

  ngOnInit() {
    this.cooperacion = this.fb.group({
      afiliado: ['', [Validators.required]],
      organismo: ['', [Validators.required]],
      beneficio: ['', [Validators.required]],
      monto: ['', [Validators.required]],
      organizacion: ['', [Validators.required]],
      frecuencia: ['', [Validators.required]],
      existe: ['', [Validators.required]],
      adquirirBienes: ['', [Validators.required]],
      queBienes: ['', [Validators.required]],
      prestacionServicios: ['', [Validators.required]],
      queServicios: ['', [Validators.required]],
      otrosProductos: ['', [Validators.required]],
      queOtrosProductos: ['', [Validators.required]],
      ecoEscala: ['', [Validators.required]]
    });
  }

  onSubmit() {

    if (!this.cooperacion.invalid) {
      this.serviceasociadosService.organizacionCooperaciones(this.cooperacion.value, this.dataInfo.nit).subscribe(data => {
        //TODO: hay que recorrer los campos
        this.serviceasociadosService.organizacionOrganismos(this.cooperacion.value, this.dataInfo.nit).subscribe(data => {
          this.serviceasociadosService.organizacionBeneficios(this.cooperacion.value, this.dataInfo.nit).subscribe(data => {

            this.serviceasociadosService.organizacionCooperacionesSolidarias(this.cooperacion.value, this.dataInfo.nit).subscribe(data => {
              this.router.navigate(['/orgotracoop']);
            }, err => console.log(err));
          }, err => console.log(err));
        }, err => console.log(err));
      }, err => console.log(err));
    }
  }

}
