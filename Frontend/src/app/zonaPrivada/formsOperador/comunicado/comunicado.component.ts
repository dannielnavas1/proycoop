import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';

@Component({
  selector: 'app-comunicado',
  templateUrl: './comunicado.component.html',
  styleUrls: ['./comunicado.component.less']
})
export class ComunicadoComponent implements OnInit {
  comunicado: FormGroup;
  dataInfo: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
              }

  ngOnInit() {
    this.comunicado = this.fb.group({
      telefono: [''],
      mensajeTexto: [''],
      boletines: [''],
      paginaweb: [''],
      correo: [''],
      redes: [''],
      video: [''],
      escritos: [''],
      radiales: [''],
      television: [''],
      revistas: [''],
      carteleras: [''],
      otro: [''],
      cual: [''],
      presencial: [''],
      present: [''],
      lineaAtencion: [''],
      buzon: [''],
      escrita: [''],
      correoElectronico: [''],
      pageWeb: [''],
      otroAsociados: [''],
      cualAsociados: [''],
      comunica: [''],
      frecuencia: [''],
      medio: [''],
      comunicaOrg: [''],
      medioUtiliza: [''],
      comunicaDesiciones: [''],
      cualEsFrecuencia: [''],
      medioutiliza2: ['']
    });
  }

  onSubmit() {
    console.log(this.comunicado.value);
    if (!this.comunicado.invalid) {
      this.serviceasociadosService.organizacionComunicacion(this.comunicado.value, this.dataInfo.nit).subscribe(data => {
        this.serviceasociadosService.organizacionComunicaOrg(this.comunicado.value, this.dataInfo.nit).subscribe(data => {
          this.serviceasociadosService.organizacionComunicacionFinancieras(this.comunicado.value, this.dataInfo.nit).subscribe(data => {
            this.serviceasociadosService.organizacionDecisionesAdmin(this.comunicado.value, this.dataInfo.nit).subscribe(data => {
              this.serviceasociadosService.organizacionDecisionesAsamblea(this.comunicado.value, this.dataInfo.nit).subscribe(data => {
                this.router.navigate(['/orgcooperacion']);
              }, err => console.log(err));
            }, err => console.log(err));
          }, err => console.log(err));
        }, err => console.log(err));
      }, err => console.log(err));
    }
  }

}
