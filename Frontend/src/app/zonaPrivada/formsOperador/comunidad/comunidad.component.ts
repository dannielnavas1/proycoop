import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';
import { DataService } from 'src/app/service/datos/data.service';

@Component({
  selector: 'app-comunidad',
  templateUrl: './comunidad.component.html',
  styleUrls: ['./comunidad.component.less']
})
export class ComunidadComponent implements OnInit {
  comunidad: FormGroup;
  dataInfo: any;
  departamentos: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private dataService: DataService,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
                this.dataService.departamento().subscribe(data => this.departamentos = data);
              }

  ngOnInit() {
    this.comunidad = this.fb.group({
      adelanta: ['', [Validators.required]],
      realiza: ['', [Validators.required]],
      caracteristica: ['', [Validators.required]],
      monto: ['', [Validators.required]],
      beneficiarios: ['', [Validators.required]],
      municipio: ['', [Validators.required]],
      impacto: ['', [Validators.required]],
      realizaProg: ['', [Validators.required]],
      progCulturales: ['', [Validators.required]],
      montoInvertido: ['', [Validators.required]],
      beneficiosCultura: ['', [Validators.required]],
      municipiosCultura: ['', [Validators.required]],
      impactoCultural: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.serviceasociadosService.organizacionComunidadesSociales(this.comunidad.value, this.dataInfo.nit).subscribe(data => {
      // TODO: Varios Registros falta enviar 
      this.serviceasociadosService.organizacionCaracteristicasSociales(this.comunidad.value, this.dataInfo.nit).subscribe(data => {
        this.serviceasociadosService.organizacionImpactoSocial(this.comunidad.value, this.dataInfo.nit).subscribe(data => {
          this.serviceasociadosService.organizacionComunicacionCultural(this.comunidad.value, this.dataInfo.nit).subscribe(data => {
            // TODO: Varios Registros falta enviar 
            this.serviceasociadosService.organizacionCaracteristicasCulturales(this.comunidad.value, this.dataInfo.nit).subscribe(data => {
              this.serviceasociadosService.organizacionImpactoCultural(this.comunidad.value, this.dataInfo.nit).subscribe(data => {
                this.router.navigate(['/orgotrascomunidad']);
              }, err => console.log(err));
            }, err => console.log(err));
          }, err => console.log(err));
        }, err => console.log(err));
      }, err => console.log(err));
    }, err => console.log(err));
  }

  


}
