import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-inicial',
  templateUrl: './inicial.component.html',
  styleUrls: ['./inicial.component.less']
})
export class InicialComponent implements OnInit {
  inicial: FormGroup;
  data: any;
  infoUser: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private spinner: NgxSpinnerService,
              private serviceasociadosService: ServiceasociadosService) {
                this.spinner.show();
              }

  ngOnInit() {
    this.inicial = this.fb.group({
      nombreEntidad: ['', [Validators.required]],
      nit: ['', [Validators.required]],
      numeroAsociados: ['', [Validators.required]],
      anho: ['', [Validators.required]]
    });

    this.infoUser = JSON.parse(this.cookie.get('userAcount'));
    console.log(this.infoUser)
    for (let u of this.infoUser.data) {
      // this.informacionbasica.controls['tipoDocumento'].setValue(u.Tipo_Documento_idTipo_Documento);
      this.inicial.controls['nit'].setValue(u.idUsers);
      this.inicial.controls['nombreEntidad'].setValue(u.Name);
      this.inicial.controls['numeroAsociados'].setValue('0');
    }
    this.onSubmit()
  }

  onSubmit() {
    if (!this.inicial.invalid) {
      this.cookie.set('organizacion', btoa(JSON.stringify(this.inicial.value)));
      this.serviceasociadosService.getSocioOrganizacion(this.inicial.value.nit).subscribe(data => {
        this.cookie.set('idIdentUser', btoa(this.inicial.value.nit));
        this.cookie.set('basico', btoa(JSON.stringify(this.inicial.value)));
        this.data = JSON.parse(atob(this.cookie.get('organizacion')));
        this.router.navigate(['/orgeconomica']);
        this.spinner.hide();
      }, err => {
        this.serviceasociadosService.socioOrganizacion(this.inicial.value)
        .subscribe(data => {
          this.data = JSON.parse(atob(this.cookie.get('organizacion')));
          this.router.navigate(['/orgeconomica']);
          this.spinner.hide();
        }, err => console.log(err));
      });
    }
  }

}
