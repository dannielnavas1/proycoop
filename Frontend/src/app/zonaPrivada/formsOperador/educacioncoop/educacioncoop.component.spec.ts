import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducacioncoopComponent } from './educacioncoop.component';

describe('EducacioncoopComponent', () => {
  let component: EducacioncoopComponent;
  let fixture: ComponentFixture<EducacioncoopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducacioncoopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducacioncoopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
