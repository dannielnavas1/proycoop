import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceasociadosService } from 'src/app/service/organizacion/serviceasociados.service';

@Component({
  selector: 'app-educacioncoop',
  templateUrl: './educacioncoop.component.html',
  styleUrls: ['./educacioncoop.component.less']
})
export class EducacioncoopComponent implements OnInit {
  educacion: FormGroup;
  dataInfo: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private serviceasociadosService: ServiceasociadosService) {
                this.dataInfo = JSON.parse(atob(this.cookie.get('organizacion')));
              }

  ngOnInit() {
    this.educacion = this.fb.group({
      educacion: ['', [Validators.required]],
      formacionBasica: ['', [Validators.required]],
      educacionFormal: ['', [Validators.required]],
      eduForNo: ['', [Validators.required]],
      invertidoEducDinatarios: ['', [Validators.required]],
      invertidoEducEmpleados: ['', [Validators.required]],
      invertidoComun: ['', [Validators.required]],
      invertidoProgs: ['', [Validators.required]],
      numEmpleados: ['', [Validators.required]],
      hombres: ['', [Validators.required]],
      mujeres: ['', [Validators.required]],
      beneficiarios: ['', [Validators.required]],
      hombresBeneficiarios: ['', [Validators.required]],
      mujeresBeneficiarios: ['', [Validators.required]],
      menoresde: ['', [Validators.required]],
      entidad: ['', [Validators.required]],
      actividad: ['', [Validators.required]],
      comunidad: ['', [Validators.required]],
    });
  }

  onSubmit() {
    if (!this.educacion.invalid) {
      this.serviceasociadosService.organizacionEducacion(this.educacion.value, this.dataInfo.nit)
        .subscribe(data => {
          this.serviceasociadosService.organizacionFormacionEmpleados(this.educacion.value, this.dataInfo.nit)
          .subscribe(data => {
            this.serviceasociadosService.organizacionFormacionExterna(this.educacion.value, this.dataInfo.nit)
            .subscribe(data => {
              // TODO: hacer llamado de los diferentes registros falta almacen para los tres casos en cada caso
              this.serviceasociadosService.organizacionActividadEducacion(this.educacion.value, this.dataInfo.nit)
                .subscribe(data => console.log(data));
              this.serviceasociadosService.organizacionComunidadEducacion(this.educacion.value, this.dataInfo.nit)
                .subscribe(data => console.log(data));
              this.serviceasociadosService.organizacionEntidadesEducacion(this.educacion.value, this.dataInfo.nit)
              .subscribe(data => {
                this.router.navigate(['/orgcomunicado']);
            }, err => console.log(err));

          }, err => console.log(err));
        }, err => console.log(err));
      }, err => console.log(err));
    }
  }

}
