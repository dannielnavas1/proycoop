import { TestBed } from '@angular/core/testing';

import { ServiceAsociadoService } from './service-asociado.service';

describe('ServiceAsociadoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceAsociadoService = TestBed.get(ServiceAsociadoService);
    expect(service).toBeTruthy();
  });
});
