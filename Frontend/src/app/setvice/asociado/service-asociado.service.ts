import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceAsociadoService {
  httpHeaders = new HttpHeaders ({
    'Content-Type': 'application/json'
  });
  params: any = {};

  constructor(private http: HttpClient) { }

  registroDatosBasicos(dataUser) {
    this.params = {
      'idSocioeconomica': dataUser.documento,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Tipopersona_idTipopersona': dataUser.tipoPersona
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/socioeconomicas', this.params, { headers: this.httpHeaders });
  }

  registroPersonaNatural(dataUser, idUser) {
    this.params = {
      'idPersona': 0,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'FechaNacimiento': dataUser.fechaNacimiento,
      'PersonasAcargo': dataUser.personasAcargo,
      'Etnia_idEtnia': dataUser.etnia,
      'EstadoCivil_idEstadoCivil': dataUser.estadoCivil,
      'Genero_idGenero': dataUser.genero,
      'SeguridadSocial_idSeguridadSocial': dataUser.seguridadSocial,
      'NivelEducativo_idNivelEducativo': dataUser.nivelEducativo,
      'Estrato_idEstrato': dataUser.estrato,
      'LiderFamiliar_idLiderFamiliar': dataUser.cabezaFamilia,
      'Actividad_idActividad': dataUser.actividadAsociado,
      'TipoVivienda_idTipoVivienda': dataUser.tipoVivienda,
      'Socioeconomica_idSocioeconomica': idUser
    }
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/personanaturales', this.params, { headers: this.httpHeaders });
  }

  registroPersonaJuridica(dataUser, idUser) {
    this.params = {
        'idPersonauridica': 0,
        'RazonSocial': dataUser.razonSocial,
        'FechaAfiliacion': dataUser.fechaAfiliación,
        'ValorBase': dataUser.valorBase,
        // 'RepresentanteLegal_idRepresentanteLegal': dataUser.,
        'Socioeconomica_idSocioeconomica': idUser,
        'TipoDerecho_idTipoDerecho': dataUser.tipoDerecho,
        'Actividad_idActividad': dataUser.actividad
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/personajuridicas', this.params, { headers: this.httpHeaders });
  }

  registroRepresentante(dataUser, idUser) {
    this.params = {
      'idRepresentanteLegal': 0,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'Telefono': dataUser.telefono,
      'Celular': dataUser.celular,
      'Correo': dataUser.correo,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/representateslegales', this.params, { headers: this.httpHeaders });
  }

  registroUbicacion(dataUser, idUser) {
    this.params = {
      'idUbicacion': 0,
      'Telefono': dataUser.telefono,
      'Celular': dataUser.celular,
      'Correo': dataUser.correo,
      'Direccion': dataUser.direccion,
      'Barrio': dataUser.barrio,
      'Socioeconomica_idSocioeconomica': idUser,
      'Ciudad_idCiudad': dataUser.ciudad,
      'Departamento_idDepartamento': dataUser.departamento,
      'Pais_idPais': dataUser.pais
    }
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/ubicaciones', this.params, { headers: this.httpHeaders });
  }

  registroBeneficiarios(dataUser, idUser) {
      this.params = {
        'idBeneficiario': dataUser.numeroDocumento,
        'PrimerNombre': dataUser.primerNombre,
        'SegundoNombre': dataUser.segundoNombre,
        'PrimerApellido': dataUser.primerApellido,
        'SegundoApellido': dataUser.segundoApellido,
        'FechaNacimiento': dataUser.fechaNacimiento,
        'FechaRetiro': dataUser.fechaRetiro === '' ? null : dataUser.fechaRetiro,
        'Cual': dataUser.cualOtra === '' ? null : dataUser.cualOtra,
        'Genero_idGenero': dataUser.genero,
        'NivelEducativo_idNivelEducativo': dataUser.nivelEducativo,
        'SeguridadSocial_idSeguridadSocial': dataUser.seguridadSocial,
        'RelNucleo_idRelNucleo': dataUser.relNucleo,
        'CausalRetiro_idCausalRetiro': dataUser.causalRetiro === '' ? null : dataUser.causalRetiro,
        'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
        'Socioeconomica_idSocioeconomica': idUser
      };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/beneficiarios', this.params, { headers: this.httpHeaders });
  }

  registroContacto(dataUser, idUser) {
    this.params = {
      'idContacto': dataUser.numeroDocumento,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'Telefono': dataUser.telefono,
      'Celular': dataUser.celular,
      'Direccion': dataUser.direccion,
      'Barrio': dataUser.barrio,
      'Pais_idPais': dataUser.pais,
      'Departamento_idDepartamento': dataUser.departamento,
      'Ciudad_idCiudad': dataUser.ciudad,
      'Area_idArea': dataUser.area,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/contactos', this.params, { headers: this.httpHeaders });
  }

  registroAsociadoEmpleo(dataUser, idUser) {
    this.params = {
      'idEmpleo': 0,
      'Nit': dataUser.nit,
      'Empresa': dataUser.empresa,
      'Direccion': dataUser.direccion,
      'Cargo': dataUser.cargo,
      'Salario': dataUser.salario,
      'FechaInicio': dataUser.fechaInicio,
      'FechaTerminacion': dataUser.fechaFinal,
      'TiempoServicioAnhos': dataUser.tiempoAños,
      'EmpleocolTiempoServicioMeses': dataUser.tiempoMeses,
      'DescuentosBase': dataUser.porcentajeDescuentoBase,
      'DescuentosAdicional': dataUser.porcentajeDescuentoAdicional,
      'Ciudad_idCiudad': dataUser.ciudad,
      'Departamento_idDepartamento': dataUser.departamento,
      'TipoContrato_idTipoContrato': dataUser.tipoContrato,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/empleos', this.params, { headers: this.httpHeaders });
  }

  registroAsociadoCursoCoop(dataUser, idUser) {
    this.params = {
      'idCursosCooperativos': 0,
      'CursosCooperativos': dataUser.curso,
      'Fecha': dataUser.fechaCooperativos,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/cursoscooperativos', this.params, { headers: this.httpHeaders });
  }

  registroAsociadoInteres(dataUser, idUser) {
    this.params = {
      'idInteres': 0,
      'Vehiculo': dataUser.poseeVehiculo,
      'OtrosIngresos': dataUser.otrosIngresos,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/interes', this.params, { headers: this.httpHeaders });
  }

  registroAsociadoAsistenciaAsamblea(dataUser, idUser) {
    this.params = {
      'idAsamblea': 0,
      'FechaAsistencia': dataUser.fechaOrdinaria,
      'Asistio': dataUser.asistio === 1 ? dataUser.asistio : 0,
      'Delego': dataUser.delego === 0 ? 1 : 0,
      'NumeroIdentificacion': dataUser.numeroDocumento,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento !== '' ? dataUser.tipoDocumento : null,
      'TipoAsamblea_idTipoAsamblea': 1,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/asambleas', this.params, { headers: this.httpHeaders });
  }

  registroAsociadoAsistenciaAsambleaDos(dataUser, idUser) {
    this.params = {
      'idAsamblea': 0,
      'FechaAsistencia': dataUser.fechaExtraordinaria,
      'Asistio': dataUser.asistioExtraordinario === 1 ? dataUser.asistioExtraordinario : 0,
      'Delego': dataUser.delegoExtraordinario === 0 ? 1 : 0,
      'NumeroIdentificacion': dataUser.numeroDocumentoExtraordinario,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumentoExtraordinario !== '' ? dataUser.tipoDocumentoExtraordinario : null,
      'TipoAsamblea_idTipoAsamblea': 2,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/asambleas', this.params, { headers: this.httpHeaders });
  }

  registroAsociadoVinculoCoop(dataUser, idUser) {
    this.params = {
      'idVinculoCooperativa': 0,
      'FechaAfiliacion': dataUser.fechaAfiliacion,
      'FechaRetiro': dataUser.fechaRetiro,
      'FechaEstado': dataUser.fechaEstado,
      'Causal': dataUser.causal,
      'Estados_idEstados': dataUser.estado,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.post<any>( environment.urlAsociado + '/api/vinculoscooperativos', this.params, { headers: this.httpHeaders });
  }

  registroFinanciero(dataUser) {
    this.params = {
      'idFinanciero': 0,
      'TipoFinancieroCreditos_idTipoFinancieroCreditos': dataUser.utilizacionCredito,
      'Cual': dataUser.cualCredito,
      'ValorDesembolso': dataUser.valorDesembolsoCredito,
      'Tasa': dataUser.tasaCredito,
      'FechaDesembolso': dataUser.fechaDesembolsoCredito,
      'TipoFinanciero_idTipoFinanciero': 1,
      'Socioeconomica_idSocioeconomica': 0
    };
    
    return this.http.post<any>( environment.urlAsociado + '/api/financieros', dataUser, { headers: this.httpHeaders });
  }

  registroEducativo(dataUser) {
    return this.http.post<any>( environment.urlAsociado + '/api/educaciones', dataUser, { headers: this.httpHeaders });
  }

  // Servicios para consultas de los datos de la persona consultada

  consultaBasico(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/socioeconomicas/' + idUser);
  }

  consultarDatos(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/personanaturales?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatosJuridica(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/personajuridicas?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatosRepredentante(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/representateslegales?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatosUbicaciones(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/ubicaciones?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatosBeneficiarios(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/beneficiarios?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatosContactos(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/contactos?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatosEmpleos(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/empleos?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatoscursosCooperativos(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/cursoscooperativos?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatoscursosInteres(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/interes?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatoscursosAsambleas(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/asambleas?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatoscursosVinculoscooperativos(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/vinculoscooperativos?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatoscursosFinancieros(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/financieros?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  consultarDatoscursosEducaciones(idUser) {
    return this.http.get<any>( environment.urlAsociado + '/api/educaciones?filter[where][Socioeconomica_idSocioeconomica]=' + idUser);
  }

  // Actualizar los datos del asociado

  actualizarDatosBasicos(dataUser) {
    this.params = {
      'idSocioeconomica': dataUser.documento,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Tipopersona_idTipopersona': dataUser.tipoPersona
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/socioeconomicas/' + dataUser.idSocioeconomica ,
      this.params, { headers: this.httpHeaders });
  }

  actualizarPersonaNatural(dataUser, idUser, idPersona) {
    this.params = {
      'idPersona': idPersona,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'FechaNacimiento': dataUser.fechaNacimiento,
      'PersonasAcargo': dataUser.personasAcargo,
      'Etnia_idEtnia': dataUser.etnia,
      'EstadoCivil_idEstadoCivil': dataUser.estadoCivil,
      'Genero_idGenero': dataUser.genero,
      'SeguridadSocial_idSeguridadSocial': dataUser.seguridadSocial,
      'NivelEducativo_idNivelEducativo': dataUser.nivelEducativo,
      'Estrato_idEstrato': dataUser.estrato,
      'LiderFamiliar_idLiderFamiliar': dataUser.cabezaFamilia,
      'Actividad_idActividad': dataUser.actividadAsociado,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/personanaturales/' + idPersona,
      this.params, { headers: this.httpHeaders });
  }

  actualizarPersonaJuridica(dataUser, idUser, idPerJur) {
    this.params = {
        'idPersonauridica': idPerJur,
        'RazonSocial': dataUser.razonSocial,
        'FechaAfiliacion': dataUser.fechaAfiliación,
        'ValorBase': dataUser.valorBase,
        // 'RepresentanteLegal_idRepresentanteLegal': dataUser.,
        'Socioeconomica_idSocioeconomica': idUser,
        'TipoDerecho_idTipoDerecho': dataUser.tipoDerecho,
        'Actividad_idActividad': dataUser.actividad
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/personajuridicas/' + idPerJur,
      this.params, { headers: this.httpHeaders });
  }

  actualizarRepresentante(dataUser, idUser, idRepre) {
    this.params = {
      'idRepresentanteLegal': idRepre,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'Telefono': dataUser.telefono,
      'Celular': dataUser.celular,
      'Correo': dataUser.correo,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/representateslegales/' + idRepre,
      this.params, { headers: this.httpHeaders });
  }

  actualizarUbicacion(dataUser, idUser, idUbicacion) {
    this.params = {
      'idUbicacion': idUbicacion,
      'Telefono': dataUser.telefono,
      'Celular': dataUser.celular,
      'Correo': dataUser.correo,
      'Direccion': dataUser.direccion,
      'Barrio': dataUser.barrio,
      'Socioeconomica_idSocioeconomica': idUser,
      'Ciudad_idCiudad': dataUser.ciudad,
      'Departamento_idDepartamento': dataUser.departamento,
      'Pais_idPais': dataUser.pais
    }
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/ubicaciones/' + idUbicacion,
      this.params, { headers: this.httpHeaders });
  }

  actualizarBeneficiarios(dataUser, idUser) {
    this.params = {
      'idBeneficiario': dataUser.numeroDocumento,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'FechaNacimiento': dataUser.fechaNacimiento,
      'FechaRetiro': dataUser.fechaRetiro,
      'Cual': dataUser.cualOtra,
      'Genero_idGenero': dataUser.genero,
      'NivelEducativo_idNivelEducativo': dataUser.nivelEducativo,
      'SeguridadSocial_idSeguridadSocial': dataUser.seguridadSocial,
      'RelNucleo_idRelNucleo': dataUser.relNucleo,
      'CausalRetiro_idCausalRetiro': dataUser.causalRetiro,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Socioeconomica_idSocioeconomica': idUser
    }
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/beneficiarios/' + dataUser.idBeneficiario,
      this.params, { headers: this.httpHeaders });
  }

  actualizarContacto(dataUser, idUser, idContacto) {
    this.params = {
      'idContacto': idContacto,
      'PrimerNombre': dataUser.primerNombre,
      'SegundoNombre': dataUser.segundoNombre,
      'PrimerApellido': dataUser.primerApellido,
      'SegundoApellido': dataUser.segundoApellido,
      'Telefono': dataUser.telefono,
      'Celular': dataUser.celular,
      'Direccion': dataUser.direccion,
      'Barrio': dataUser.barrio,
      'Pais_idPais': dataUser.pais,
      'Departamento_idDepartamento': dataUser.departamento,
      'Ciudad_idCiudad': dataUser.ciudad,
      'Area_idArea': dataUser.area,
      'Tipocodumento_idTipocodumento': dataUser.tipoDocumento,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/contactos/' + idContacto,
      this.params, { headers: this.httpHeaders });
  }

  actualizarAsociadoEmpleo(dataUser, idUser) {
    this.params = {
      'idEmpleo': 0,
      'Nit': dataUser.nit,
      'Empresa': dataUser.empresa,
      'Direccion': dataUser.direccion,
      'Cargo': dataUser.cargo,
      'Salario': dataUser.salario,
      'FechaInicio': dataUser.fechaInicio,
      'FechaTerminacion': dataUser.fechaFinal,
      'TiempoServicioAnhos': dataUser.tiempoAños,
      'EmpleocolTiempoServicioMeses': dataUser.tiempoMeses,
      'DescuentosBase': dataUser.porcentajeDescuentoBase,
      'DescuentosAdicional': dataUser.porcentajeDescuentoAdicional,
      'Ciudad_idCiudad': dataUser.ciudad,
      'Departamento_idDepartamento': dataUser.departamento,
      'TipoContrato_idTipoContrato': dataUser.tipoContrato,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/empleos/' + dataUser.idEmpleo,
      this.params, { headers: this.httpHeaders });
  }

  actualizarAsociadoCursoCoop(dataUser, idUser) {
    this.params = {
      'idCursosCooperativos': 0,
      'CursosCooperativos': dataUser.curso,
      'Fecha': dataUser.fechaCooperativos,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/cursoscooperativos/' + dataUser.idCursosCooperativos,
      this.params, { headers: this.httpHeaders });
  }

  actualizarAsociadoInteres(dataUser, idUser) {
    this.params = {
      'idInteres': 0,
      'Vehiculo': dataUser.poseeVehiculo,
      'OtrosIngresos': dataUser.otrosIngresos,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/interes/' + dataUser.idInteres,
      this.params, { headers: this.httpHeaders });
  }

  actualizarAsociadoAsistenciaAsamblea(dataUser, idUser) {
    this.params = {
      'idAsamblea': 0,
      'FechaAsistencia': dataUser.fechaOrdinaria,
      'Asistio': dataUser.asistio,
      'Delego': dataUser.delego,
      'NumeroIdentificacion': dataUser.numeroDocumento,
      'TipoAsamblea_idTipoAsamblea': 1,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/asambleas/' + dataUser.idAsamblea,
      this.params, { headers: this.httpHeaders });
  }

  actualizarAsociadoVinculoCoop(dataUser, idUser) {
    this.params = {
      'idVinculoCooperativa': 0,
      'FechaAfiliacion': dataUser.fechaAfiliacion,
      'FechaRetiro': dataUser.fechaRetiro,
      'FechaEstado': dataUser.fechaEstado,
      'Causal': dataUser.causal,
      'Estados_idEstados': dataUser.estado,
      'Socioeconomica_idSocioeconomica': idUser
    };
    console.log(this.params);
    return this.http.put<any>( environment.urlAsociado + '/api/vinculoscooperativos/' + dataUser.idVinculoCooperativa,
      this.params, { headers: this.httpHeaders });
  }

  actualizarFinanciero(dataUser) {
    this.params = {
      'idFinanciero': 0,
      'Tipo': dataUser.utilizacionCredito,
      'Cual': dataUser.cualCredito,
      'ValorDesembolso': dataUser.valorDesembolsoCredito,
      'Tasa': dataUser.tasaCredito,
      'FechaDesembolso': dataUser.fechaDesembolsoCredito,
      'TipoFinanciero_idTipoFinanciero': 1,
      'Socioeconomica_idSocioeconomica': 0
    };
    return this.http.put<any>( environment.urlAsociado + '/api/financieros/' + dataUser.idFinanciero,
    dataUser, { headers: this.httpHeaders });
  }

  actualizarEducativo(dataUser) {
    return this.http.put<any>( environment.urlAsociado + '/api/educaciones/' + dataUser.idFinanciero,
      dataUser, { headers: this.httpHeaders });
  }
}
