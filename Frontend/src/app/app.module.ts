import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatrialModule } from './material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from "@angular/forms";
import { NgxSpinnerModule } from "ngx-spinner";

// import { AngularFireModule } from "@angular/fire";
// import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from 'src/environments/environment.prod';
import { IndexappComponent } from './user/indexapp/indexapp.component';
import { ServiceWorkerModule } from '@angular/service-worker';
// import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { RecoverPasswordComponent } from './user/recover-password/recover-password.component';
import { RecoverypasswordComponent } from './user/recoverypassword/recoverypassword.component';
import { PanelComponent } from './zonaPrivada/panel/panel.component';
import { InformacionPersonalComponent } from './zonaPrivada/informacion-personal/informacion-personal.component';
import { AdministrarComponent } from './zonaPrivada/administrar/administrar.component';
import { InformesComponent } from './zonaPrivada/informes/informes.component';
import { InformacionbasicaComponent } from './zonaPrivada/forms/informacionbasica/informacionbasica.component';
import { PersonanaturalComponent } from './zonaPrivada/forms/personanatural/personanatural.component';
import { PersonajuridicaComponent } from './zonaPrivada/forms/personajuridica/personajuridica.component';
import { UbicacionComponent } from './zonaPrivada/forms/ubicacion/ubicacion.component';
import { BeneficiariosComponent } from './zonaPrivada/forms/beneficiarios/beneficiarios.component';
import { PersonacontactoComponent } from './zonaPrivada/forms/personacontacto/personacontacto.component';
import { AsociadoComponent } from './zonaPrivada/forms/asociado/asociado.component';
import { FinancieroComponent } from './zonaPrivada/forms/financiero/financiero.component';
import { EducacionComponent } from './zonaPrivada/forms/educacion/educacion.component';
import { InicialComponent } from './zonaPrivada/formsOperador/inicial/inicial.component';
import { EconomicaComponent } from './zonaPrivada/formsOperador/economica/economica.component';
import { EducacioncoopComponent } from './zonaPrivada/formsOperador/educacioncoop/educacioncoop.component';
import { ComunicadoComponent } from './zonaPrivada/formsOperador/comunicado/comunicado.component';
import { CooperacionComponent } from './zonaPrivada/formsOperador/cooperacion/cooperacion.component';
import { OtrascooperacionComponent } from './zonaPrivada/formsOperador/otrascooperacion/otrascooperacion.component';
import { ComunidadComponent } from './zonaPrivada/formsOperador/comunidad/comunidad.component';
import { OtrascomunidadComponent } from './zonaPrivada/formsOperador/otrascomunidad/otrascomunidad.component';
import { OtrosComponent } from './zonaPrivada/formsOperador/otros/otros.component';
import { EvaluacionComponent } from './zonaPrivada/formsOperador/evaluacion/evaluacion.component';
import { CardInfoAsociadoComponent } from './zonaPrivada/forms/complement/card-info-asociado/card-info-asociado.component';
import { FijoInfoComponent } from './zonaPrivada/formsOperador/components/fijo-info/fijo-info.component';
import { RegisterComponent } from './user/register/register.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexappComponent,
    RecoverPasswordComponent,
    RecoverypasswordComponent,
    PanelComponent,
    InformacionPersonalComponent,
    AdministrarComponent,
    InformesComponent,
    InformacionbasicaComponent,
    PersonanaturalComponent,
    PersonajuridicaComponent,
    UbicacionComponent,
    BeneficiariosComponent,
    PersonacontactoComponent,
    AsociadoComponent,
    FinancieroComponent,
    EducacionComponent,
    InicialComponent,
    EconomicaComponent,
    EducacioncoopComponent,
    ComunicadoComponent,
    CooperacionComponent,
    OtrascooperacionComponent,
    ComunidadComponent,
    OtrascomunidadComponent,
    OtrosComponent,
    EvaluacionComponent,
    CardInfoAsociadoComponent,
    FijoInfoComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    // AngularFireModule.initializeApp(environment.firebaseConfig),
    // AngularFirestoreModule,
    MatrialModule,
    HttpClientModule,
    NgxSpinnerModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    CookieService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
