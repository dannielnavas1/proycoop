import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexappComponent } from './user/indexapp/indexapp.component';
import { RecoverPasswordComponent } from './user/recover-password/recover-password.component';
import { RecoverypasswordComponent } from './user/recoverypassword/recoverypassword.component';
import { PanelComponent } from './zonaPrivada/panel/panel.component';
import { AuthGuard } from './guards/auth.guard';
import { InformesComponent } from './zonaPrivada/informes/informes.component';
import { InformacionPersonalComponent } from './zonaPrivada/informacion-personal/informacion-personal.component';
import { AdministrarComponent } from './zonaPrivada/administrar/administrar.component';
import { InformacionbasicaComponent } from './zonaPrivada/forms/informacionbasica/informacionbasica.component';
import { PersonanaturalComponent } from './zonaPrivada/forms/personanatural/personanatural.component';
import { PersonajuridicaComponent } from './zonaPrivada/forms/personajuridica/personajuridica.component';
import { UbicacionComponent } from './zonaPrivada/forms/ubicacion/ubicacion.component';
import { BeneficiariosComponent } from './zonaPrivada/forms/beneficiarios/beneficiarios.component';
import { PersonacontactoComponent } from './zonaPrivada/forms/personacontacto/personacontacto.component';
import { AsociadoComponent } from './zonaPrivada/forms/asociado/asociado.component';
import { FinancieroComponent } from './zonaPrivada/forms/financiero/financiero.component';
import { EducacionComponent } from './zonaPrivada/forms/educacion/educacion.component';
import { InicialComponent } from './zonaPrivada/formsOperador/inicial/inicial.component';
import { EconomicaComponent } from './zonaPrivada/formsOperador/economica/economica.component';
import { EducacioncoopComponent } from './zonaPrivada/formsOperador/educacioncoop/educacioncoop.component';
import { ComunicadoComponent } from './zonaPrivada/formsOperador/comunicado/comunicado.component';
import { CooperacionComponent } from './zonaPrivada/formsOperador/cooperacion/cooperacion.component';
import { OtrascooperacionComponent } from './zonaPrivada/formsOperador/otrascooperacion/otrascooperacion.component';
import { ComunidadComponent } from './zonaPrivada/formsOperador/comunidad/comunidad.component';
import { OtrascomunidadComponent } from './zonaPrivada/formsOperador/otrascomunidad/otrascomunidad.component';
import { OtrosComponent } from './zonaPrivada/formsOperador/otros/otros.component';
import { EvaluacionComponent } from './zonaPrivada/formsOperador/evaluacion/evaluacion.component';
import { RegisterComponent } from './user/register/register.component';

const routes: Routes = [
  {
    path: '',
    component: IndexappComponent
  },
  {
    path: 'registro',
    component: RegisterComponent
  },
  {
    path: 'recover',
    component: RecoverPasswordComponent
  },
  {
    path: 'recovery/:id',
    component: RecoverypasswordComponent
  },
  {
    path: 'panel',
    component: PanelComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'informes',
    component: InformesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'informacion',
    component: InformacionPersonalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'administracion',
    component: AdministrarComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'basica',
    component: InformacionbasicaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'natural',
    component: PersonanaturalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'juridica',
    component: PersonajuridicaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'ubicacion',
    component: UbicacionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'beneficiarios',
    component: BeneficiariosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contacto',
    component: PersonacontactoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'asociado',
    component: AsociadoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'financiero',
    component: FinancieroComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'educacion',
    component: EducacionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orginicial',
    component: InicialComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgeconomica',
    component: EconomicaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgeducacion',
    component: EducacioncoopComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgcomunicado',
    component: ComunicadoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgcooperacion',
    component: CooperacionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgotracoop',
    component: OtrascooperacionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgcomunidades',
    component: ComunidadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgotrascomunidad',
    component: OtrascomunidadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgotros',
    component: OtrosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orgevaluaciones',
    component: EvaluacionComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
