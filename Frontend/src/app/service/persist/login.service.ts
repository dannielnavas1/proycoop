import { Injectable } from '@angular/core';
import { Auth } from '../../models/persist/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private isUserLoggedIn;
  public UserLogged: Auth;
  userLogget: string;
  constructor() {
    this.isUserLoggedIn = false;
  }

  setUserLoggedIn(user: Auth){
    this.isUserLoggedIn = true;
    this.UserLogged = user;
    sessionStorage.setItem('currentUser', btoa(this.isUserLoggedIn.idUser));
  }

  getUserLoggedIn() {
    this.userLogget = sessionStorage.getItem('currentUser');
    return atob(this.userLogget);
  }

}
