import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  data: any;

  constructor(private http: HttpClient,
              private cookie: CookieService) { }

  getDataId() {
    this.data = JSON.parse(atob(this.cookie.get('basico')));
    return this.data.documento;
  }

  departamento() {
    return this.http.get<any>('https://colombia.globalenterprise.com.co/departamentos');
  }

  pais() {
    return this.http.get<any>('https://colombia.globalenterprise.com.co/pais');
  }

  ciudad(idDepartamento) {
    let params = {
      'Departamento': idDepartamento
    };
    return this.http.post<any>('https://colombia.globalenterprise.com.co/searchmunicipios', params);
  }

}
