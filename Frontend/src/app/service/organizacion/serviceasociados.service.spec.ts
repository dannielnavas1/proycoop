import { TestBed } from '@angular/core/testing';

import { ServiceasociadosService } from './serviceasociados.service';

describe('ServiceasociadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceasociadosService = TestBed.get(ServiceasociadosService);
    expect(service).toBeTruthy();
  });
});
