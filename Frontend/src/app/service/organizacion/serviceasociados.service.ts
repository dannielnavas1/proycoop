import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceasociadosService {
  httpHeaders = new HttpHeaders ({
    'Content-Type': 'application/json'
  });
  params: any = {};

  constructor(private http: HttpClient) { }

  socioOrganizacion(dataUser) {
    this.params = {
      'idSocioEconomicaOrganizacion': dataUser.nit,
      'NombreEntidad': dataUser.nombreEntidad,
      'Anho': dataUser.numeroAsociados,
      'NumeroAsociados': dataUser.anho
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/sociosorganizaciones',
      this.params, {headers: this.httpHeaders});
  }

  organizacionEconomica(dataUser, idUser) {
    this.params = {
      'AhorrosPermanentes': dataUser.ahorros,
      'AportesSociales': dataUser.aportes,
      'AportesNoReducibles': dataUser.reducibles,
      'MontoCierre': dataUser.monto,
      'TotalFondosInvertidos': dataUser.total,
      'ExedenteAnhoAnterior': dataUser.exedente,
      'DistribucionVigilancia': dataUser.distribucion,
      'FondoEducacion': dataUser.fondoEducativo,
      'FondoSolidaridad': dataUser.fondoSolidaridad,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/economicas',
    this.params, {headers: this.httpHeaders});
  }

  organizacionParticipacionEconomica(dataUser, idUser) {
    this.params = {
      'FondoBienestar': dataUser.fondoBienestar,
      'FondoDesarrolloEmpresarial': dataUser.fondoDesarrollo,
      'OtroFondo': dataUser.otroFondo,
      'Cual': dataUser.cual,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/participacionesasociados',
    this.params, {headers: this.httpHeaders});
  }

  organizacionAutonomia(dataUser) {
    return this.http.post<any>( environment.urlOrganizacion + '/api/autonomiaindependencias',
    dataUser, {headers: this.httpHeaders});
  }

  organizacionEducacion(dataUser, idUser) {
    this.params = {
      'EducacionFormacion': dataUser.educacion,
      'FormacionBasica': dataUser.formacionBasica,
      'EducacionFormal': dataUser.educacionFormal,
      'EducacionNoFormal': dataUser.eduForNo,
      'InvertidoDinatarios': dataUser.invertidoEducDinatarios,
      'InvertidoEmpleados': dataUser.invertidoEducEmpleados,
      'InvertidoExtra': dataUser.invertidoComun,
      'InvertidoEmprendimiento': dataUser.invertidoProgs,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/educaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionFormacionEmpleados(dataUser, idUser) {
    this.params = {
      "NumeroEmpleados": dataUser.numEmpleados,
      "Hombres": dataUser.hombres,
      "Mujeres": dataUser.mujeres,
      "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/formacionesempleados',
    this.params, {headers: this.httpHeaders});
  }

  organizacionFormacionExterna(dataUser, idUser) {
    this.params = {
      'Beneficiarios': dataUser.beneficiarios,
      'HombresBeneficiarios': dataUser.hombresBeneficiarios,
      'MujeresBeneficiarios': dataUser.mujeresBeneficiarios,
      'MenoresEdad': dataUser.menoresde,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/formacionesexternas',
    this.params, {headers: this.httpHeaders});
  }

  organizacionEntidadesEducacion(dataUser, idUser) {
    this.params = {
      'EntidadesEducacion': dataUser.entidad,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/entidadeseducaciones',
    this.params, { headers: this.httpHeaders });
  }

  organizacionActividadEducacion(dataUser, idUser) {
    this.params = {
      'ActividadesEmprendimiento': dataUser.actividad,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/actividadesemprendimientos',
    this.params, { headers: this.httpHeaders });
  }

  organizacionComunidadEducacion(dataUser, idUser) {
    this.params = {
      'ComunidadExterna': dataUser.comunidad,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunidadesexternas',
    this.params, { headers: this.httpHeaders });
  }

  organizacionComunicacion(dataUser, idUser) {
    this.params = {
      'Telefono': dataUser.telefono,
      'MensajeTexto': dataUser.mensajeTexto,
      'Boletines': dataUser.boletines,
      'PaginaWeb': dataUser.paginaweb,
      'CorreoElectronico': dataUser.correo,
      'RedesSociales': dataUser.redes,
      'VideoConferencia': dataUser.video,
      'MediosEscritos': dataUser.escritos,
      'MediosRadiales': dataUser.radiales,
      'Television': dataUser.television,
      'RevistasLibros': dataUser.revistas,
      'Carteleras': dataUser.carteleras,
      'Otro': dataUser.otro,
      'Cual': dataUser.cual,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunicaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionComunicaOrg(dataUser, idUser) {
    this.params = {
        'Presencial': dataUser.presencial,
        'Presencial2': dataUser.present,
        'LineaAtencion': dataUser.lineaAtencion,
        'BuzonSugerencias': dataUser.buzon,
        'Escrita': dataUser.escrita,
        'CorreoElectronicoAsociado': dataUser.correoElectronico,
        'PaginaWebAsociado': dataUser.pageWeb,
        'OtroAsociado': dataUser.otroAsociados,
        'CualOtroAsociado': dataUser.cualAsociados,
        'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/asociadoscomunicacionesorgs',
    this.params, {headers: this.httpHeaders});
  }

  organizacionComunicacionFinancieras(dataUser, idUser) {
    this.params = {
      'Comunica': dataUser.comunica,
      'CualFrecuencia': dataUser.frecuencia,
      'CualMedioUtiliza': dataUser.medio,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunicacionesfinancieras',
    this.params, {headers: this.httpHeaders});
  }

  organizacionDecisionesAdmin(dataUser, idUser) {
    this.params = {
      "ComunicaAdmon": dataUser.comunicaDesiciones,
      "CualMedioUtilizaAdmon": dataUser.medioutiliza2,
      'CualFrecuencia': dataUser.cualEsFrecuencia,
      "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/decisionesadmis',
    this.params, {headers: this.httpHeaders});
  }

  organizacionDecisionesAsamblea(dataUser, idUser) {
    this.params = {
      'comunicaAsambleas': dataUser.comunicaOrg,
      'CualMedioUtilizaAsamblea': dataUser.medioUtiliza,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/decisionesadmis',
    this.params, {headers: this.httpHeaders});
  }

  organizacionCooperaciones(dataUser, idUser) {
    this.params = {
      "Afiliado": dataUser.afiliado,
      "MontoAportes": dataUser.monto,
      "OrganizacionAsisteAsamblea": dataUser.organizacion,
      "MontoAportesOrganos": null,
      "FrecuenciaAsisteAsamblea": dataUser.frecuencia,
      "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/cooperaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionOrganismos(dataUser, idUser) {
    this.params = {
      'Organismos': dataUser.organismo,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/organismos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionBeneficios(dataUser, idUser) {
    this.params = {
      'Beneficios': dataUser.beneficio,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/beneficios',
    this.params, {headers: this.httpHeaders});
  }

  organizacionCooperacionesSolidarias(dataUser, idUser) {
    this.params = {
      'ExisteCooperacion': dataUser.existe,
      'ParaAdquirirBienes': dataUser.adquirirBienes,
      'QueBienesAdquire': dataUser.queBienes,
      'ParaPrestacionServicios': dataUser.prestacionServicios,
      'QueServicioAdquire': dataUser.queServicios,
      'ParaAdquirirOtrosProdServ': dataUser.otrosProductos,
      'QueProdServ': dataUser.queOtrosProductos,
      'AlianzaGenEsc': dataUser.ecoEscala,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/cooperacionessolidarias',
    this.params, {headers: this.httpHeaders});
  }

  organizacionOtrasCooperaciones(dataUser, idUser) {
    this.params = {
      'Economicos': dataUser.economicos ? dataUser.economicos : false,
      'Educativos': dataUser.educativos ? dataUser.educativos : false,
      'CulturalesRecreativos': dataUser.culturales ? dataUser.culturales : false,
      'TipoAcuerdo': dataUser.tiposAcuerdo,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/otrascooperaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionOrganizacionalesInter(dataUser, idUser) {
    this.params = {
      'Menbresia': dataUser.membresia,
      'Entidad': dataUser.entidad,
      'TieneAcuerdos': dataUser.acuerdos,
      Pais_idPais: dataUser.pais,
      SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion: idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/organizacionesinternacionales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionAcuerdos(dataUser, idUser) {
    this.params = {
      "Acuerdos": dataUser.acuerdo,
      "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/acuerdos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionObjetivos(dataUser, idUser) {
    this.params = {
      Objetivos: dataUser.objetivo,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/objetivos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionComunidadesSociales(dataUser, idUser) {
    this.params = {
      'AdelantaProgramas': dataUser.adelanta,
      'RealizaProgramas': dataUser.realiza,
      'MontoInvertido': dataUser.monto,
      'NoBeneficiarios': dataUser.beneficiarios,
      'Municipios_idMunicipios': dataUser.municipio,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunidadessociales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionCaracteristicasSociales(dataUser, idUser) {
    this.params = {
      "Objetivos": dataUser.caracteristica,
      "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/caracteristicassociales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionImpactoSocial(dataUser, idUser) {
    this.params = {
      'ImpactoSocial': dataUser.impacto,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/impactossociales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionComunicacionCultural(dataUser, idUser) {
    this.params = {
      'RealizaProgramasCulturales': dataUser.realizaProg,
      'MontoInvertidoCultural': dataUser.montoInvertido,
      'NoBeneficiariosCultural': dataUser.beneficiosCultura,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser,
      'Municipios_idMunicipios': dataUser.municipiosCultura
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunicacionessociales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionCaracteristicasCulturales(dataUser, idUser) {
    this.params = {
      Objetivos: dataUser.progCulturales,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/caracteristicasculturales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionImpactoCultural(dataUser, idUser) {
    this.params = {
      'ImpactoCultural': dataUser.impactoCultural,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/impactosculturales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionComunidadAmbiente(dataUser, idUser) {
    this.params = {
      'AdelantaProgramasAmbientes': dataUser.adelanta,
      'MontoInvertidoAmbiente': dataUser.monto,
      'NoBeneficiariosAmbiente': dataUser.beneficiarios,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser,
      'Municipios_idMunicipios': dataUser.municipio
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunicacionessociales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionCaracteristicasAmbiental(dataUser, idUser) {
    this.params = {
      'CaracteristicasAmbiental': dataUser.caracteristica,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/caracteristicasambientales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionImpactoAmbiental(dataUser, idUser) {
    this.params = {
      'ImpactoAmbiental': dataUser.impacto,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/impactosambientales',
    this.params, {headers: this.httpHeaders});
  }

  organizacionComunidadDesarrollo(dataUser, idUser) {
    this.params = {
      'RealizaProgramasEcoComun': dataUser.realizaProg,
      'MontoInvertidoEcoComun': dataUser.montoInvertido,
      'NoBeneficiariosEcoComun': dataUser.beneficiosCultura,
      'Municipios_idMunicipios': dataUser.municipiosCultura,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/comunidadesdesarrollos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionCaracteristicasEcoComun(dataUser, idUser) {
    this.params = {
      'Objetivos': dataUser.progCulturales,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/caracteristicasenonomicas',
    this.params, {headers: this.httpHeaders});
  }

  organizacionImpactoEcoComun(dataUser, idUser) {
    this.params = {
      'ImpactoEcoComun': dataUser.impactoCultural,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    }
    return this.http.post<any>( environment.urlOrganizacion + '/api/impactoseconomicos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionOtrasComunidad(dataUser, idUser) {
    this.params = {
      'Participa': dataUser.participa,
      'ValorAportes': dataUser.valor,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/otrascomunidades',
    this.params, {headers: this.httpHeaders});
  }

  organizacionOtros(dataUser, idUser) {
    this.params = {
      'DesarrolladoRelaciones': dataUser.desarrollado,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/otros',
    this.params, {headers: this.httpHeaders});
  }

  organizacionRelaciones(dataUser, idUser) {
    this.params = {
      'AlianzasRel': dataUser.alianza,
      'AcuerdosRel': dataUser.acuerdos,
      'ConveniosRel': dataUser.convenios,
      'TipoRelaciones_idTipoRelaciones': 1,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/relaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionRelacionesDos(dataUser, idUser) {
    this.params = {
      'AlianzasRel': dataUser.alianzaPrivadas,
      'AcuerdosRel': dataUser.acuerdosPrivadas,
      'ConveniosRel': dataUser.conveniosPrivadas,
      'TipoRelaciones_idTipoRelaciones': 2,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/relaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionRelacionesTres(dataUser, idUser) {
    this.params = {
      'AlianzasRel': dataUser.alianzaInternacionales,
      'AcuerdosRel': dataUser.acuerdosInternacionales,
      'ConveniosRel': dataUser.conveniosInternacionales,
      'TipoRelaciones_idTipoRelaciones': 3,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/relaciones',
    this.params, {headers: this.httpHeaders});
  }

  organizacionObjetivosCompromiso(dataUser, idUser) {
    this.params = {
      "InstPublica": dataUser.publicas,
      "InstPrivada": dataUser.privadas,
      "InstInternal": dataUser.internal,
      "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/objetivoscompromisos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionAlianzas(dataUser, idUser) {
    this.params = {
      'MontoInvertidoCompromiso': dataUser.monton,
      'NoBeneficiariosCompromiso': dataUser.beneficiarios,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/alianzas',
    this.params, {headers: this.httpHeaders});
  }

  organizacionImpactoCompromiso(dataUser, idUser) {
    this.params = {
      'InstPublicaImp': dataUser.publicasDesarrollar,
      'InstPrivadaImp': dataUser.privadasDesarrollar,
      'InstInternalImp': dataUser.internalDesarrollar,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/impactoscompromisos',
    this.params, {headers: this.httpHeaders});
  }

  organizacionEvaluacion(dataUser, idUser) {
    this.params = {
      'RealizaEvaluacion': dataUser.evaluacion,
      'Balance': dataUser.balance,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
    };
    return this.http.post<any>( environment.urlOrganizacion + '/api/impactoscompromisos',
    this.params, {headers: this.httpHeaders});
  }


  // Consultas Organizaciones

  getSocioOrganizacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/sociosorganizaciones/' + idUser);
  }

  getOrganizacionEconomica(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/economicas?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

  getOrganizacionParticipacionEconomica(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/participacionesasociados?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }
  // Revisar
  getOrganizacionAutonomia(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/autonomiaindependencias?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser );
  }
  getOrganizacionEducacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/educaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

  getOrganizacionFormacionEmpleados(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/formacionesempleados?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

  getOrganizacionFormacionExterna(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/formacionesexternas?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

  getOrganizacionEntidadesEducacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/entidadeseducaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

  getOrganizacionActividadEducacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/actividadesemprendimientos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunidadEducacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunidadesexternas?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunicacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunicaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunicaOrg(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/asociadoscomunicacionesorgs?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunicacionFinancieras(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunicacionesfinancieras?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionDecisionesAdmin(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/decisionesadmis?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionDecisionesAsamblea(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/decisionesadmis?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionCooperaciones(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/cooperaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionOrganismos(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/organismos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionBeneficios(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/beneficios?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionCooperacionesSolidarias(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/cooperacionessolidarias?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionOtrasCooperaciones(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/otrascooperaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionOrganizacionalesInter(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/organizacionesinternacionales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionAcuerdos(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/acuerdos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionObjetivos(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/objetivos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunidadesSociales(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunidadessociales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionCaracteristicasSociales(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/caracteristicassociales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionImpactoSocial(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/impactossociales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunicacionCultural(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunicacionessociales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionCaracteristicasCulturales(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/caracteristicasculturales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionImpactoCultural(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/impactosculturales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunidadAmbiente(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunicacionessociales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionCaracteristicasAmbiental(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/caracteristicasambientales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionImpactoAmbiental(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/impactosambientales?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionComunidadDesarrollo(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/comunidadesdesarrollos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionCaracteristicasEcoComun(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/caracteristicasenonomicas?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionImpactoEcoComun(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/impactoseconomicos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionOtrasComunidad(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/otrascomunidades?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionOtros(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/otros?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionRelaciones(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/relaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionRelacionesDos(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/relaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionRelacionesTres(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/relaciones?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionObjetivosCompromiso(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/objetivoscompromisos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionAlianzas(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/alianzas?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionImpactoCompromiso(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/impactoscompromisos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }

getOrganizacionEvaluacion(idUser) {
    return this.http.get<any>( environment.urlOrganizacion +
      '/api/impactoscompromisos?filter[where][SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion]=' + idUser);
  }



//Actualizar organizaciones

putSocioOrganizacion(dataUser, idUser, idRepre) {
  this.params = {
    'idSocioEconomicaOrganizacion': dataUser.nit,
    'NombreEntidad': dataUser.nombreEntidad,
    'Anho': dataUser.numeroAsociados,
    'NumeroAsociados': dataUser.anho
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/sociosorganizaciones/' + idRepre,
    this.params, {headers: this.httpHeaders});
}

putOrganizacionEconomica(dataUser, idUser, idRepre) {
  this.params = {
    'AhorrosPermanentes': dataUser.ahorros,
    'AportesSociales': dataUser.aportes,
    'AportesNoReducibles': dataUser.reducibles,
    'MontoCierre': dataUser.monto,
    'TotalFondosInvertidos': dataUser.total,
    'ExedenteAnhoAnterior': dataUser.exedente,
    'DistribucionVigilancia': dataUser.distribucion,
    'FondoEducacion': dataUser.fondoEducativo,
    'FondoSolidaridad': dataUser.fondoSolidaridad,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/economicas/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionParticipacionEconomica(dataUser, idUser, idRepre) {
  this.params = {
    'FondoBienestar': dataUser.fondoBienestar,
    'FondoDesarrolloEmpresarial': dataUser.fondoDesarrollo,
    'OtroFondo': dataUser.otroFondo,
    'Cual': dataUser.cual,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/participacionesasociados/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionAutonomia(dataUser, idRepre) {
  return this.http.put<any>( environment.urlOrganizacion + '/api/autonomiaindependencias/' + idRepre,
  dataUser, {headers: this.httpHeaders});
}

putOrganizacionEducacion(dataUser, idUser, idRepre) {
  this.params = {
    'EducacionFormacion': dataUser.educacion,
    'FormacionBasica': dataUser.formacionBasica,
    'EducacionFormal': dataUser.educacionFormal,
    'EducacionNoFormal': dataUser.eduForNo,
    'InvertidoDinatarios': dataUser.invertidoEducDinatarios,
    'InvertidoEmpleados': dataUser.invertidoEducEmpleados,
    'InvertidoExtra': dataUser.invertidoComun,
    'InvertidoEmprendimiento': dataUser.invertidoProgs,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/educaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionFormacionEmpleados(dataUser, idUser, idRepre) {
  this.params = {
    "NumeroEmpleados": dataUser.numEmpleados,
    "Hombres": dataUser.hombres,
    "Mujeres": dataUser.mujeres,
    "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/formacionesempleados/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionFormacionExterna(dataUser, idUser, idRepre) {
  this.params = {
    'Beneficiarios': dataUser.beneficiarios,
    'HombresBeneficiarios': dataUser.hombresBeneficiarios,
    'MujeresBeneficiarios': dataUser.mujeresBeneficiarios,
    'MenoresEdad': dataUser.menoresde,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/formacionesexternas/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionEntidadesEducacion(dataUser, idUser, idRepre) {
  this.params = {
    'EntidadesEducacion': dataUser.entidad,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/entidadeseducaciones/' + idRepre,
  this.params, { headers: this.httpHeaders });
}

putOrganizacionActividadEducacion(dataUser, idUser, idRepre) {
  this.params = {
    'ActividadesEmprendimiento': dataUser.actividad,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/actividadesemprendimientos/' + idRepre,
  this.params, { headers: this.httpHeaders });
}

putOrganizacionComunidadEducacion(dataUser, idUser, idRepre) {
  this.params = {
    'ComunidadExterna': dataUser.comunidad,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunidadesexternas/' + idRepre,
  this.params, { headers: this.httpHeaders });
}

putOrganizacionComunicacion(dataUser, idUser, idRepre) {
  this.params = {
    'Telefono': dataUser.telefono,
    'MensajeTexto': dataUser.mensajeTexto,
    'Boletines': dataUser.boletines,
    'PaginaWeb': dataUser.paginaweb,
    'CorreoElectronico': dataUser.correo,
    'RedesSociales': dataUser.redes,
    'VideoConferencia': dataUser.video,
    'MediosEscritos': dataUser.escritos,
    'MediosRadiales': dataUser.radiales,
    'Television': dataUser.television,
    'RevistasLibros': dataUser.revistas,
    'Carteleras': dataUser.carteleras,
    'Otro': dataUser.otro,
    'Cual': dataUser.cual,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunicaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionComunicaOrg(dataUser, idUser, idRepre) {
  this.params = {
      'Presencial': dataUser.presencial,
      'Presencial2': dataUser.present,
      'LineaAtencion': dataUser.lineaAtencion,
      'BuzonSugerencias': dataUser.buzon,
      'Escrita': dataUser.escrita,
      'CorreoElectronicoAsociado': dataUser.correoElectronico,
      'PaginaWebAsociado': dataUser.pageWeb,
      'OtroAsociado': dataUser.otroAsociados,
      'CualOtroAsociado': dataUser.cualAsociados,
      'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/asociadoscomunicacionesorgs/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionComunicacionFinancieras(dataUser, idUser, idRepre) {
  this.params = {
    'Comunica': dataUser.comunica,
    'CualFrecuencia': dataUser.frecuencia,
    'CualMedioUtiliza': dataUser.medio,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunicacionesfinancieras/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionDecisionesAdmin(dataUser, idUser, idRepre) {
  this.params = {
    "ComunicaAdmon": dataUser.comunicaDesiciones,
    "CualMedioUtilizaAdmon": dataUser.medioutiliza2,
    'CualFrecuencia': dataUser.cualEsFrecuencia,
    "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/decisionesadmis/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionDecisionesAsamblea(dataUser, idUser, idRepre) {
  this.params = {
    'comunicaAsambleas': dataUser.comunicaOrg,
    'CualMedioUtilizaAsamblea': dataUser.medioUtiliza,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/decisionesadmis/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionCooperaciones(dataUser, idUser, idRepre) {
  this.params = {
    "Afiliado": dataUser.afiliado,
    "MontoAportes": dataUser.monto,
    "OrganizacionAsisteAsamblea": dataUser.organizacion,
    "MontoAportesOrganos": null,
    "FrecuenciaAsisteAsamblea": dataUser.frecuencia,
    "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/cooperaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionOrganismos(dataUser, idUser, idRepre) {
  this.params = {
    'Organismos': dataUser.organismo,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/organismos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionBeneficios(dataUser, idUser, idRepre) {
  this.params = {
    'Beneficios': dataUser.beneficio,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/beneficios/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionCooperacionesSolidarias(dataUser, idUser, idRepre) {
  this.params = {
    'ExisteCooperacion': dataUser.existe,
    'ParaAdquirirBienes': dataUser.adquirirBienes,
    'QueBienesAdquire': dataUser.queBienes,
    'ParaPrestacionServicios': dataUser.prestacionServicios,
    'QueServicioAdquire': dataUser.queServicios,
    'ParaAdquirirOtrosProdServ': dataUser.otrosProductos,
    'QueProdServ': dataUser.queOtrosProductos,
    'AlianzaGenEsc': dataUser.ecoEscala,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/cooperacionessolidarias/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionOtrasCooperaciones(dataUser, idUser, idRepre) {
  this.params = {
    'Economicos': dataUser.economicos ? dataUser.economicos : false,
    'Educativos': dataUser.educativos ? dataUser.educativos : false,
    'CulturalesRecreativos': dataUser.culturales ? dataUser.culturales : false,
    'TipoAcuerdo': dataUser.tiposAcuerdo,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/otrascooperaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionOrganizacionalesInter(dataUser, idUser, idRepre) {
  this.params = {
    'Menbresia': dataUser.membresia,
    'Entidad': dataUser.entidad,
    'TieneAcuerdos': dataUser.acuerdos,
    Pais_idPais: dataUser.pais,
    SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion: idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/organizacionesinternacionales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionAcuerdos(dataUser, idUser, idRepre) {
  this.params = {
    "Acuerdos": dataUser.acuerdo,
    "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/acuerdos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionObjetivos(dataUser, idUser, idRepre) {
  this.params = {
    Objetivos: dataUser.objetivo,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/objetivos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionComunidadesSociales(dataUser, idUser, idRepre) {
  this.params = {
    'AdelantaProgramas': dataUser.adelanta,
    'RealizaProgramas': dataUser.realiza,
    'MontoInvertido': dataUser.monto,
    'NoBeneficiarios': dataUser.beneficiarios,
    'Municipios_idMunicipios': dataUser.municipio,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunidadessociales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionCaracteristicasSociales(dataUser, idUser, idRepre) {
  this.params = {
    "Objetivos": dataUser.caracteristica,
    "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/caracteristicassociales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionImpactoSocial(dataUser, idUser, idRepre) {
  this.params = {
    'ImpactoSocial': dataUser.impacto,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/impactossociales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionComunicacionCultural(dataUser, idUser, idRepre) {
  this.params = {
    'RealizaProgramasCulturales': dataUser.realizaProg,
    'MontoInvertidoCultural': dataUser.montoInvertido,
    'NoBeneficiariosCultural': dataUser.beneficiosCultura,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser,
    'Municipios_idMunicipios': dataUser.municipiosCultura
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunicacionessociales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionCaracteristicasCulturales(dataUser, idUser, idRepre) {
  this.params = {
    Objetivos: dataUser.progCulturales,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/caracteristicasculturales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionImpactoCultural(dataUser, idUser, idRepre) {
  this.params = {
    'ImpactoCultural': dataUser.impactoCultural,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/impactosculturales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionComunidadAmbiente(dataUser, idUser, idRepre) {
  this.params = {
    'AdelantaProgramasAmbientes': dataUser.adelanta,
    'MontoInvertidoAmbiente': dataUser.monto,
    'NoBeneficiariosAmbiente': dataUser.beneficiarios,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser,
    'Municipios_idMunicipios': dataUser.municipio
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunicacionessociales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionCaracteristicasAmbiental(dataUser, idUser, idRepre) {
  this.params = {
    'CaracteristicasAmbiental': dataUser.caracteristica,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/caracteristicasambientales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionImpactoAmbiental(dataUser, idUser, idRepre) {
  this.params = {
    'ImpactoAmbiental': dataUser.impacto,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/impactosambientales/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionComunidadDesarrollo(dataUser, idUser, idRepre) {
  this.params = {
    'RealizaProgramasEcoComun': dataUser.realizaProg,
    'MontoInvertidoEcoComun': dataUser.montoInvertido,
    'NoBeneficiariosEcoComun': dataUser.beneficiosCultura,
    'Municipios_idMunicipios': dataUser.municipiosCultura,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/comunidadesdesarrollos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionCaracteristicasEcoComun(dataUser, idUser, idRepre) {
  this.params = {
    'Objetivos': dataUser.progCulturales,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/caracteristicasenonomicas/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionImpactoEcoComun(dataUser, idUser, idRepre) {
  this.params = {
    'ImpactoEcoComun': dataUser.impactoCultural,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  }
  return this.http.put<any>( environment.urlOrganizacion + '/api/impactoseconomicos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionOtrasComunidad(dataUser, idUser, idRepre) {
  this.params = {
    'Participa': dataUser.participa,
    'ValorAportes': dataUser.valor,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/otrascomunidades/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionOtros(dataUser, idUser, idRepre) {
  this.params = {
    'DesarrolladoRelaciones': dataUser.desarrollado,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/otros/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionRelaciones(dataUser, idUser, idRepre) {
  this.params = {
    'AlianzasRel': dataUser.alianza,
    'AcuerdosRel': dataUser.acuerdos,
    'ConveniosRel': dataUser.convenios,
    'TipoRelaciones_idTipoRelaciones': 1,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/relaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionRelacionesDos(dataUser, idUser, idRepre) {
  this.params = {
    'AlianzasRel': dataUser.alianzaPrivadas,
    'AcuerdosRel': dataUser.acuerdosPrivadas,
    'ConveniosRel': dataUser.conveniosPrivadas,
    'TipoRelaciones_idTipoRelaciones': 2,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/relaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionRelacionesTres(dataUser, idUser, idRepre) {
  this.params = {
    'AlianzasRel': dataUser.alianzaInternacionales,
    'AcuerdosRel': dataUser.acuerdosInternacionales,
    'ConveniosRel': dataUser.conveniosInternacionales,
    'TipoRelaciones_idTipoRelaciones': 3,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/relaciones/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionObjetivosCompromiso(dataUser, idUser, idRepre) {
  this.params = {
    "InstPublica": dataUser.publicas,
    "InstPrivada": dataUser.privadas,
    "InstInternal": dataUser.internal,
    "SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion": idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/objetivoscompromisos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionAlianzas(dataUser, idUser, idRepre) {
  this.params = {
    'MontoInvertidoCompromiso': dataUser.monton,
    'NoBeneficiariosCompromiso': dataUser.beneficiarios,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/alianzas/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionImpactoCompromiso(dataUser, idUser, idRepre) {
  this.params = {
    'InstPublicaImp': dataUser.publicasDesarrollar,
    'InstPrivadaImp': dataUser.privadasDesarrollar,
    'InstInternalImp': dataUser.internalDesarrollar,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/impactoscompromisos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

putOrganizacionEvaluacion(dataUser, idUser, idRepre) {
  this.params = {
    'RealizaEvaluacion': dataUser.evaluacion,
    'Balance': dataUser.balance,
    'SocioEconomicaOrganizacion_idSocioEconomicaOrganizacion': idUser
  };
  return this.http.put<any>( environment.urlOrganizacion + '/api/impactoscompromisos/' + idRepre,
  this.params, {headers: this.httpHeaders});
}

}
