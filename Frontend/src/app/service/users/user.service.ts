import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  params: any;
  constructor(private http: HttpClient) { }

  createUser(userdata) {

    const httpHeaders = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Q-i6BOixVbZcr9HkMMJzdnwDstM1GtAENXgI2SZJDj4' 
    });

    let nombredos = userdata.nombreDos === null ? '' : userdata.nombreDos;

    this.params = {
      'idUsers': userdata.idUsers,
      'Email': userdata.Email,
      'Name': userdata.nombre +' '+ nombredos +' '+ userdata.apellidoPrimero +' '+ userdata.apellidoSegundo,
      'Password': userdata.Password,
      'Tipo_Documento_idTipo_Documento': userdata.TipoDocumento,
      'Roles_idRoles': userdata.Roles_idRoles,
      'TipoPersona_idTipoPersona': userdata.TipoPersona_idTipoPersona
  }
    
    return this.http.post<any>( environment.urlUser + '/api/createusers', this.params,  { headers: httpHeaders })
  }

  loginUser(userdata) {
    const httpHeaders = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Q-i6BOixVbZcr9HkMMJzdnwDstM1GtAENXgI2SZJDj4' 
    });

    return this.http.post<any>( environment.urlUser + '/api/auth', userdata, { headers: httpHeaders })
  }

  recoveryPassword(userdata) {
    const httpHeaders = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Q-i6BOixVbZcr9HkMMJzdnwDstM1GtAENXgI2SZJDj4' 
    });
    return this.http.post<any>( environment.urlUser + '/api/recovery', userdata, { headers: httpHeaders })
  }
}
