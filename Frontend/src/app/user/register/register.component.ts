import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/persist/login.service';
import { UserService } from 'src/app/service/users/user.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  registro: FormGroup;
  siteKey: string = '6LeZLKwUAAAAAKF7D4vtqTMhPaCdmpMZOUG_D3SO';
  datos: any;
  resp: boolean;
  verContraseña: boolean = false;
  iconPassword: string = 'visibility_off';
  typeInput: string = 'password';
  verReContraseña: boolean = false;
  iconRePassword: string = 'visibility_off';
  typeInputRe: string = 'password';
  verContraseñaLogin: boolean = false;
  iconPasswordLogin: string = 'visibility_off';
  typeInputLogin: string = 'password';

  constructor(private cookie: CookieService,
              private fb: FormBuilder,
              private router: Router,
              private loginService: LoginService,
              private userService: UserService) { }

  ngOnInit() {
    this.registro = this.fb.group({
      TipoDocumento: ['', [Validators.required]],
      idUsers: ['', [Validators.required]],
      // UserName: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      nombreDos: ['', [Validators.required]],
      apellidoPrimero: ['', [Validators.required]],
      apellidoSegundo: [''],
      Email: ['', [Validators.required, Validators.email]],
      Password: ['', [Validators.required]],
      RepeatPassword: ['', [Validators.required]],
      Roles_idRoles: [1],
      TipoPersona_idTipoPersona: ['', Validators.required]
    });
  }


  onSubmit(){
    this.userService.createUser(this.registro.value).subscribe(data =>{
      console.log(data);
      
      Swal.fire(
        data.mensaje === 'El usuario ya se encuentra registrado' ? 'Información' : 'Proceso exitoso',
        data.mensaje, 
        data.mensaje === 'El usuario ya se encuentra registrado' ? 'info' : 'success',
        );
    }, error => {
      Swal.fire('Error',
        error, 
        'info');
      console.log(error);
    })
  }

  viewPassword(){
    this.verContraseña = !this.verContraseña;
    if(this.verContraseña){
      this.iconPassword = 'visibility';
      this.typeInput = 'text';
    } else {
      this.iconPassword = 'visibility_off';
      this.typeInput = 'password';
    }
  }

  viewRePassword(){
    this.verReContraseña = !this.verReContraseña;
    if(this.verReContraseña){
      this.iconRePassword = 'visibility';
      this.typeInputRe = 'text';
    } else {
      this.iconRePassword = 'visibility_off';
      this.typeInputRe = 'password';
    }
  }

}
