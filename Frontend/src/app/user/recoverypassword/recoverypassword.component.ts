import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-recoverypassword',
  templateUrl: './recoverypassword.component.html',
  styleUrls: ['./recoverypassword.component.less']
})
export class RecoverypasswordComponent implements OnInit {
  recoveryPassword: FormGroup;
  verContraseña: boolean = false;
  iconPassword: string = 'visibility_off';
  typeInput: string = 'password';
  verReContraseña: boolean = false;
  iconRePassword: string = 'visibility_off';
  typeInputRe: string = 'password';

  constructor(private fb: FormBuilder) { }

  viewPassword(){
    this.verContraseña = !this.verContraseña;
    if(this.verContraseña){
      this.iconPassword = 'visibility';
      this.typeInput = 'text';
    } else {
      this.iconPassword = 'visibility_off';
      this.typeInput = 'password';
    }
  }

  viewRePassword(){
    this.verReContraseña = !this.verReContraseña;
    if(this.verReContraseña){
      this.iconRePassword = 'visibility';
      this.typeInputRe = 'text';
    } else {
      this.iconRePassword = 'visibility_off';
      this.typeInputRe = 'password';
    }
  }

  ngOnInit() {
    this.recoveryPassword = this.fb.group({
      password: ['', [Validators.required]],
      rePassword: ['', [Validators.required]]
    })
  }

}
