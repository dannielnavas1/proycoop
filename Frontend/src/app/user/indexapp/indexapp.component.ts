import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/users/user.service';
import { CookieService } from 'ngx-cookie-service';
import { Auth } from '../../models/persist/auth';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/persist/login.service';

import Swal from 'sweetalert2';


@Component({
  selector: 'app-indexapp',
  templateUrl: './indexapp.component.html',
  styleUrls: ['./indexapp.component.less']
})
export class IndexappComponent implements OnInit {
  registro: FormGroup;
  login: FormGroup;
  siteKey: string = '6LeZLKwUAAAAAKF7D4vtqTMhPaCdmpMZOUG_D3SO';
  datos: any;
  resp: boolean;
  verContraseña: boolean = false;
  iconPassword: string = 'visibility_off';
  typeInput: string = 'password';
  verReContraseña: boolean = false;
  iconRePassword: string = 'visibility_off';
  typeInputRe: string = 'password';
  verContraseñaLogin: boolean = false;
  iconPasswordLogin: string = 'visibility_off';
  typeInputLogin: string = 'password';

  constructor(private cookie: CookieService,
              private fb: FormBuilder,
              private router: Router,
              private loginService: LoginService,
              private userService: UserService) { }

  onSubmitLogin(){
    this.userService.loginUser(this.login.value).subscribe(data =>{
      this.datos = data;
      if (this.datos.data.length !== 0) {
          let u: Auth = this.datos.idUsers; 
          this.loginService.setUserLoggedIn(u);
          this.cookie.set('coopIdent', btoa('true'));
          this.cookie.set('userAcount', JSON.stringify(this.datos));
          // console.log(this.cookie.get('userAcount'));
          this.router.navigate(['/panel']);
      } else if (this.datos.data.length === 0) {
          this.resp = true;
      }
    }, error => {
      console.log(error);
      Swal.fire('Tenemos un inconveniente!',
        'Usuario y/o contraseña invalidos.', 
        'info');
    });
  }

  

  viewPasswordLogin(){
    this.verContraseñaLogin = !this.verContraseñaLogin;
    if(this.verContraseñaLogin){
      this.iconPasswordLogin = 'visibility';
      this.typeInputLogin = 'text';
    } else {
      this.iconPasswordLogin = 'visibility_off';
      this.typeInputLogin = 'password';
    }
  }

  ngOnInit() {
    this.login = this.fb.group({
      idUsers: ['', [Validators.required]],
      Password: ['', [Validators.required]],
      recaptcha: ['', Validators.required]
    })
  }

}
