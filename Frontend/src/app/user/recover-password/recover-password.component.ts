import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/users/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.less']
})
export class RecoverPasswordComponent implements OnInit {
  recuperar: FormGroup;

  constructor(private fb: FormBuilder,
              private userService: UserService) { }

  onSubmit() {
    console.log(this.recuperar.value);
    if (!this.recuperar.invalid) {
      this.userService.recoveryPassword(this.recuperar.value).subscribe(data => {
        Swal.fire('Exito!',
        data.mensaje, 
        'success');
      }, err => {
        Swal.fire('Upps!',
        'No encontramos su usuario por favor valide los datos ingresados', 
        'error');
      })
    }
  }

  ngOnInit() {
    this.recuperar = this.fb.group({
      Tipo_Documento_idTipo_Documento: ['', [Validators.required]],
      idUsers: ['', [Validators.required, Validators.maxLength(16)]]
    })
  }

}
