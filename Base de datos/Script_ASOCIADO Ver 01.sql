/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 14.1 		*/
/*  Created On : 08-jul.-2019 12:50:15 				*/
/*  DBMS       : MySql 						*/
/* ---------------------------------------------------- */

SET FOREIGN_KEY_CHECKS=0
; 
/* Drop Tables */

DROP TABLE IF EXISTS `TASOC1` CASCADE
;

DROP TABLE IF EXISTS `TASOC2` CASCADE
;

DROP TABLE IF EXISTS `TBENEF` CASCADE
;

DROP TABLE IF EXISTS `TCONTR` CASCADE
;

DROP TABLE IF EXISTS `TDIREC` CASCADE
;

DROP TABLE IF EXISTS `TEDUC1` CASCADE
;

DROP TABLE IF EXISTS `TFINA1` CASCADE
;

DROP TABLE IF EXISTS `TPARMS` CASCADE
;

/* Create Tables */

CREATE TABLE `TASOC1`
(
	`A1_TIPID` VARCHAR(3) NOT NULL,
	`A1_NUMID` INT NOT NULL,
	`A1_DIGVER` INT NULL,
	`A1_TIPPER` VARCHAR(30) NOT NULL,
	`A1_PRINOM` VARCHAR(50) NOT NULL,
	`A1_SEGNOM` VARCHAR(50) NULL,
	`A1_PRIAPE` VARCHAR(50) NOT NULL,
	`A1_SEGAPE` VARCHAR(50) NULL,
	`A1_TIPJUR` VARCHAR(30) NULL,
	`A1_FECNAC` DATE NULL,
	`A1_GENERO` VARCHAR(30) NOT NULL,
	`A1_ETNIA` VARCHAR(30) NULL,
	`A1_NIVEDU` VARCHAR(30) NULL,
	`A1_ESTCIV` VARCHAR(30) NULL,
	`A1_ESTRATO` VARCHAR(20) NOT NULL,
	`A1_SSOCIAL` VARCHAR(20) NOT NULL,
	`A1_TIPVIV` VARCHAR(20) NOT NULL,
	`A1_LIDER` VARCHAR(30) NOT NULL,
	`A1_NUMPER` INT NULL,
	`A1_ACTIV` VARCHAR(20) NOT NULL,
	`A1_TEL` VARCHAR(30) NULL,
	`A1_CEL` VARCHAR(30) NOT NULL,
	`A1_CORREO` VARCHAR(50) NULL,
	CONSTRAINT `PK_TASOC1` PRIMARY KEY (`A1_NUMID` ASC)
)

;

CREATE TABLE `TASOC2`
(
	`A2_TIPID` VARCHAR(3) NOT NULL,
	`A2_NUMID` INT NOT NULL,
	`A2_DIGVER` INT NULL,
	`A2_INDVEHI` BOOL NULL,
	`A2_OINGRE` INT NULL,
	`A2_INDORDIN` BOOL NULL,
	`A2_INDDELEGO` BOOL NULL,
	`A2_TIPIDORDIN` VARCHAR(3) NULL,
	`A2_NUMIDORDIN` INT NULL,
	`A2_INDEXTRA` BOOL NULL,
	`A2_INDDELEXTRA` BOOL NULL,
	`A2_TIPIDEXTRA` VARCHAR(3) NULL,
	`A2_NUMIDEXTRA` INT NULL,
	`A2_ESTADO` VARCHAR(20) NOT NULL,
	`A2_FECAFILIA` DATE NULL,
	`A2_FECRETIRO` DATE NULL,
	`A2_FECESTADO` DATE NULL,
	`A2_CAUSAL` VARCHAR(20) NULL,
	`A1_NUMID` INT NULL,
	CONSTRAINT `PK_TASOC2` PRIMARY KEY (`A2_NUMID` ASC)
)

;

CREATE TABLE `TBENEF`
(
	`BE_TIPID` VARCHAR(3) NOT NULL,
	`BE_NUMID` INT NOT NULL,
	`BE_TIPIDBEN` VARCHAR(3) NOT NULL,
	`BE_NUMIDBEN` INT NOT NULL,
	`BE_TIPREL` VARCHAR(20) NOT NULL,
	`BE_INDCON` BOOL NULL,
	`BE_PRINOM` VARCHAR(50) NOT NULL,
	`BE_SEGNOM` VARCHAR(50) NULL,
	`BE_PRIAPE` VARCHAR(50) NOT NULL,
	`BE_SEGAPE` VARCHAR(50) NULL,
	`BE_FECNAC` DATE NOT NULL,
	`BE_GENERO` VARCHAR(30) NOT NULL,
	`BE_RELNUC` VARCHAR(30) NOT NULL,
	`BE_NIVEDU` VARCHAR(30) NOT NULL,
	`BE_SSOCIAL` VARCHAR(20) NOT NULL,
	`BE_FECRET` DATE NULL,
	`BE_CAUSAL` VARCHAR(20) NULL,
	`BE_TEL` VARCHAR(30) NULL,
	`BE_CEL` VARCHAR(30) NULL,
	`BE_CORREO` VARCHAR(50) NULL,
	`A1_NUMID` INT NULL,
	CONSTRAINT `PK_TBENEF` PRIMARY KEY (`BE_NUMIDBEN` ASC)
)

;

CREATE TABLE `TCONTR`
(
	`CO_TIPID` VARCHAR(3) NOT NULL,
	`CO_NUMID` INT NOT NULL,
	`CO_NIT` INT NOT NULL,
	`CO_DIGVER` INT NULL,
	`CO_EMPRESA` VARCHAR(50) NULL,
	`CO_NOMCARGO` VARCHAR(50) NOT NULL,
	`CO_NOMDEPEN` VARCHAR(50) NOT NULL,
	`CO_TIPCON` VARCHAR(30) NULL,
	`CO_FECINICON` DATE NOT NULL,
	`CO_FECFINCON` DATE NULL,
	`CO_SALARIO` INT NOT NULL,
	`CO_PORDES` DOUBLE(3,2) NULL,
	`CO_POROVAL` DOUBLE(3,2) NULL,
	`A1_NUMID` INT NULL,
	`DI_NUMID` INT NULL,
	`DI_SEC` INT NULL,
	CONSTRAINT `PK_TCONTR` PRIMARY KEY (`CO_NUMID` ASC, `CO_NIT` ASC)
)

;

CREATE TABLE `TDIREC`
(
	`DI_TIPID` VARCHAR(3) NOT NULL,
	`DI_NUMID` INT NOT NULL,
	`DI_DIGVER` INT NULL,
	`DI_SEC` INT NOT NULL,
	`DI_TIPREL` VARCHAR(20) NOT NULL,
	`DI_DIREC` VARCHAR(50) NOT NULL,
	`DI_AREA` VARCHAR(20) NOT NULL,
	`DI_BARRIO` VARCHAR(50) NULL,
	`DI_CIUDAD` VARCHAR(50) NOT NULL,
	`DI_DEPTO` VARCHAR(50) NULL,
	`DI_PAIS` VARCHAR(50) NULL,
	`DI_ESTADO` BOOL NOT NULL,
	`A1_NUMID` INT NULL,
	`BE_NUMIDBEN` INT NULL,
	CONSTRAINT `PK_TDIREC` PRIMARY KEY (`DI_NUMID` ASC, `DI_SEC` ASC)
)

;

CREATE TABLE `TEDUC1`
(
	`E1_TIPID` VARCHAR(3) NOT NULL,
	`E1_NUMID` INT NULL,
	`E1_FECHA` DATE NULL,
	`E1_CURSO` VARCHAR(50) NULL,
	`E1_CODIGO` VARCHAR(20) NULL,
	`A1_NUMID` INT NULL,
	`BE_NUMIDBEN` INT NULL
)

;

CREATE TABLE `TFINA1`
(
	`F1_TIPIDE` VARCHAR(3) NOT NULL,
	`F1_NUMIDE` INT NOT NULL,
	`F1_DIGVER` INT NULL,
	`F1_CODIGO` VARCHAR(2) NOT NULL,
	`F1_USO` VARCHAR(50) NOT NULL,
	`F1_OTROUSO` VARCHAR(50) NULL,
	`F1_VALOR` INT NULL,
	`F1_TASA` DOUBLE(3,2) NULL,
	`F1_FECHA` DATE NOT NULL,
	`F1_CANOPE` INT NULL,
	`A1_NUMID` INT NULL
)

;

CREATE TABLE `TPARMS`
(
	`PA_PPIO` VARCHAR(10) NULL,
	`PA_SECCION` INT NULL,
	`PA_CONSEC` INT NULL,
	`PA_TIPO` VARCHAR(50) NULL,
	`PA_VALOR` VARCHAR(50) NULL,
	`PA_CODIGO` VARCHAR(20) NULL
)

;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE `TASOC1` 
 ADD CONSTRAINT `constraint2` UNIQUE (`A1_CORREO` ASC)
;

ALTER TABLE `TASOC1` 
 ADD INDEX `constraint1` (`A1_PRIAPE` ASC)
;

ALTER TABLE `TASOC2` 
 ADD INDEX `IXFK_TASOC2_TASOC1` (`A1_NUMID` ASC)
;

ALTER TABLE `TBENEF` 
 ADD INDEX `IXFK_TBENEF_TASOC1` (`A1_NUMID` ASC)
;

ALTER TABLE `TCONTR` 
 ADD INDEX `IXFK_TCONTR_TASOC1` (`A1_NUMID` ASC)
;

ALTER TABLE `TCONTR` 
 ADD INDEX `IXFK_TCONTR_TDIREC` (`DI_NUMID` ASC, `DI_SEC` ASC)
;

ALTER TABLE `TDIREC` 
 ADD INDEX `IXFK_TDIREC_TASOC1` (`A1_NUMID` ASC)
;

ALTER TABLE `TDIREC` 
 ADD INDEX `IXFK_TDIREC_TBENEF` (`BE_NUMIDBEN` ASC)
;

ALTER TABLE `TDIREC` 
 ADD INDEX `constraint1` (`DI_DIREC` ASC)
;

ALTER TABLE `TEDUC1` 
 ADD INDEX `IXFK_TEDUC1_TASOC1` (`A1_NUMID` ASC)
;

ALTER TABLE `TEDUC1` 
 ADD INDEX `IXFK_TEDUC1_TBENEF` (`BE_NUMIDBEN` ASC)
;

ALTER TABLE `TEDUC1` 
 ADD INDEX `constraint1` (`E1_NUMID` ASC)
;

ALTER TABLE `TFINA1` 
 ADD INDEX `IXFK_TFINA1_TASOC1` (`A1_NUMID` ASC)
;

/* Create Foreign Key Constraints */

ALTER TABLE `TASOC2` 
 ADD CONSTRAINT `FK_TASOC2_TASOC1`
	FOREIGN KEY (`A1_NUMID`) REFERENCES `TASOC1` (`A1_NUMID`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TBENEF` 
 ADD CONSTRAINT `FK_TBENEF_TASOC1`
	FOREIGN KEY (`A1_NUMID`) REFERENCES `TASOC1` (`A1_NUMID`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TCONTR` 
 ADD CONSTRAINT `FK_TCONTR_TASOC1`
	FOREIGN KEY (`A1_NUMID`) REFERENCES `TASOC1` (`A1_NUMID`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TCONTR` 
 ADD CONSTRAINT `FK_TCONTR_TDIREC`
	FOREIGN KEY (`DI_NUMID`, `DI_SEC`) REFERENCES `TDIREC` (`DI_NUMID`,`DI_SEC`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TDIREC` 
 ADD CONSTRAINT `FK_TDIREC_TASOC1`
	FOREIGN KEY (`A1_NUMID`) REFERENCES `TASOC1` (`A1_NUMID`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TDIREC` 
 ADD CONSTRAINT `FK_TDIREC_TBENEF`
	FOREIGN KEY (`BE_NUMIDBEN`) REFERENCES `TBENEF` (`BE_NUMIDBEN`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TDIREC` 
 ADD CONSTRAINT `constraint2`
	FOREIGN KEY (`DI_NUMID`) REFERENCES  () ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE `TEDUC1` 
 ADD CONSTRAINT `FK_TEDUC1_TASOC1`
	FOREIGN KEY (`A1_NUMID`) REFERENCES `TASOC1` (`A1_NUMID`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TEDUC1` 
 ADD CONSTRAINT `FK_TEDUC1_TBENEF`
	FOREIGN KEY (`BE_NUMIDBEN`) REFERENCES `TBENEF` (`BE_NUMIDBEN`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `TFINA1` 
 ADD CONSTRAINT `FK_TFINA1_TASOC1`
	FOREIGN KEY (`A1_NUMID`) REFERENCES `TASOC1` (`A1_NUMID`) ON DELETE Restrict ON UPDATE Restrict
;

SET FOREIGN_KEY_CHECKS=1
; 
